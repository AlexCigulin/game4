import { Direction, PositionRange, Size, TilePosition } from '@game/Types';
import { normalizeBetweenTwoBounds } from '@game/Helper';

export function areTilePositionsEquals(TilePosition1: TilePosition, TilePosition2: TilePosition): boolean {
    return TilePosition1.x === TilePosition2.x && TilePosition1.y === TilePosition2.y;
}

export function areTilePositionsAdjacent(TilePosition1: TilePosition, TilePosition2: TilePosition): boolean {
    if (TilePosition1.x === TilePosition2.x) {
        if (TilePosition1.y - 1 === TilePosition2.y || TilePosition1.y + 1 === TilePosition2.y) {
            return true;
        }
    }

    if (TilePosition1.y === TilePosition2.y) {
        if (TilePosition1.x - 1 === TilePosition2.x || TilePosition1.x + 1 === TilePosition2.x) {
            return true;
        }
    }

    return false;
}

export function getDirection(from: TilePosition, to: TilePosition): Direction {
    switch (true) {
        case to.x < from.x && to.y < from.y:
            return Direction.NW;
        case to.x === from.x && to.y < from.y:
            return Direction.N;
        case to.x > from.x && to.y < from.y:
            return Direction.NE;
        case to.x > from.x && to.y === from.y:
            return Direction.E;
        case to.x > from.x && to.y > from.y:
            return Direction.SE;
        case to.x === from.x && to.y > from.y:
            return Direction.S;
        case to.x < from.x && to.y > from.y:
            return Direction.SW;
        case to.x < from.x && to.y === from.y:
            return Direction.W;
        default:
            throw new Error();
    }
}

export function getOppositeDirection(direction: Direction): Direction {
    switch (direction) {
        case Direction.N:
            return Direction.S;
        case Direction.NE:
            return Direction.SW;
        case Direction.E:
            return Direction.W;
        case Direction.SE:
            return Direction.NW;
        case Direction.S:
            return Direction.N;
        case Direction.SW:
            return Direction.NE;
        case Direction.W:
            return Direction.E;
        case Direction.NW:
            return Direction.SE;
        default:
            throw new Error();
    }
}

export function isDiagonalDirection(direction: Direction): boolean {
    switch (direction) {
        case Direction.NE:
            // no break
        case Direction.SE:
            // no break
        case Direction.SW:
            // no break
        case Direction.NW:
            return true;
        default:
            return false;
    }
}

export function getDirections(includeDiagonal: boolean = true): Direction[] {
    const directions = [
        Direction.W,
        Direction.NW,
        Direction.N,
        Direction.NE,
        Direction.E,
        Direction.SE,
        Direction.S,
        Direction.SW,
    ];

    if (includeDiagonal) {
        return directions;
    }

    return directions.filter((direction: Direction): boolean => {
        return !isDiagonalDirection(direction);
    });
}

export function getAdjacentTilePositionRange(tile: TilePosition, distance: number, mapSize: Size): PositionRange {
    const fromX = normalizeBetweenTwoBounds(tile.x - distance, 0, mapSize.width - 1);
    const toX = normalizeBetweenTwoBounds(tile.x + distance, 0, mapSize.width - 1);
    const fromY = normalizeBetweenTwoBounds(tile.y - distance, 0, mapSize.height - 1);
    const toY = normalizeBetweenTwoBounds(tile.y + distance, 0, mapSize.height - 1);

    return {
        fromX,
        toX,
        fromY,
        toY,
    };
}
