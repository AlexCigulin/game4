import { AStarFinder, AStarFinderInterface, DiagonalMovement, GridInterface } from '@game/Adapters';
import { TargetPosition, TilePosition } from '@game/Types';
import { getDirection } from '@game/Map/Helper';

class PathFinder {
    private finder: AStarFinderInterface;

    public constructor() {
        this.finder = new AStarFinder({ diagonalMovement: DiagonalMovement.ONLY_WHEN_NO_OBSTACLES });
    }

    public findPath(grid: GridInterface, from: TilePosition, to: TilePosition): TargetPosition[] | null {
        const result = this.finder.findPath(from.x, from.y, to.x, to.y, grid);

        if (!result) {
            return null;
        }

        const path: TargetPosition[] = [];

        for (let i = 0; i < result.length; i++) {
            if (i === 0) {
                // First step equals "from" parameter, it should be excluded:
                continue;
            }

            const previousX = result[i - 1][0];
            const previousY = result[i - 1][1];

            const currentX = result[i][0];
            const currentY = result[i][1];

            const direction = getDirection({ x: previousX, y: previousY }, { x: currentX, y: currentY });

            path.push({
                direction,
                x: currentX,
                y: currentY,
            });
        }

        return path;
    }
}

export default PathFinder;
