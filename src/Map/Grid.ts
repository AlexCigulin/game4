import { Tilemaps } from 'phaser';
import { Callback, ModelInterface, TilePosition } from '@game/Types';
import { GridInterface, Grid as PathFinderGrid } from '@game/Adapters';

enum CELL_CONTENT {
    OBSTACLE = 'obstacle',
    NOTHING = 'nothing',
}

type CellContent = ModelInterface | CELL_CONTENT;
type ModelRegister = {
    [id: string]: TilePosition;
};

class Grid {
    private matrix!: CellContent[][];
    private servedModels: ModelRegister = {};

    public constructor(map: Tilemaps.Tilemap, obstaclesLayer: Tilemaps.TilemapLayer) {
        this.initTileMap(map, obstaclesLayer);
    }

    public addModel(model: ModelInterface, initialPosition: TilePosition): void {
        this.servedModels[model.getId()] = initialPosition;
        this.matrix[initialPosition.y][initialPosition.x] = model;
    }

    public removeModel(model: ModelInterface): void {
        const position = this.getModelPosition(model);

        this.matrix[position.y][position.x] = CELL_CONTENT.NOTHING;
        delete this.servedModels[model.getId()];
    }

    public hasModel(model: ModelInterface): boolean {
        return this.servedModels[model.getId()] !== undefined;
    }

    public getModelPosition(model: ModelInterface): TilePosition {
        if (!this.hasModel(model)) {
            throw new Error('Cannot get position for model with id: ' + model.getId());
        }

        return Object.assign({}, this.servedModels[model.getId()]);
    }

    public getModelByPosition(position: TilePosition): ModelInterface | null {
        const content = this.matrix[position.y][position.x];

        if (content === CELL_CONTENT.NOTHING || content === CELL_CONTENT.OBSTACLE) {
            return null;
        }

        return content;
    }

    public updateModelPosition(model: ModelInterface, newPosition: TilePosition): void {
        const oldPosition = this.getModelPosition(model);

        this.matrix[oldPosition.y][oldPosition.x] = CELL_CONTENT.NOTHING;
        this.matrix[newPosition.y][newPosition.x] = model;
        this.servedModels[model.getId()] = newPosition;
    }

    public hasObstacleOnPosition(position: TilePosition): boolean {
        return this.matrix[position.y][position.x] === CELL_CONTENT.OBSTACLE;
    }

    public isFreeOnPosition(position: TilePosition): boolean {
        return this.matrix[position.y][position.x] === CELL_CONTENT.NOTHING;
    }

    public freePosition(position: TilePosition): Callback {
        const tileContent = this.matrix[position.y][position.x];
        this.matrix[position.y][position.x] = CELL_CONTENT.NOTHING;

        return (): void => {
            this.matrix[position.y][position.x] = tileContent;
        };
    }

    public getGrid(): GridInterface {
        const normalizedTileMap: number[][] = [];

        for (let y = 0; y < this.matrix.length; y++) {
            normalizedTileMap[y] = [];

            for (let x = 0; x < this.matrix[y].length; x++) {
                const isFree: boolean = this.matrix[y][x] === CELL_CONTENT.NOTHING;
                normalizedTileMap[y][x] = isFree ? 0 : 1;
            }
        }

        return new PathFinderGrid(normalizedTileMap);
    }

    public toString(): string {
        let string = "";

        for (let y = 0; y < this.matrix.length; y++) {
            for (let x = 0; x < this.matrix[y].length; x++) {
                switch (this.matrix[y][x]) {
                    case CELL_CONTENT.OBSTACLE:
                        string += "O ";
                        break;
                    case CELL_CONTENT.NOTHING:
                        string += ". ";
                        break;
                    default:
                        string += "@ ";
                }
            }

            string += "\n";
        }

        return string;
    }

    private initTileMap(map: Tilemaps.Tilemap, obstaclesLayer: Tilemaps.TilemapLayer): void {
        this.matrix = [];

        for (let y = 0; y < map.height; y++) {
            const columns: CellContent[] = [];

            for (let x = 0; x < map.width; x++) {
                const cellContent: CellContent = obstaclesLayer.getTileAt(x, y, false) === null
                    ? CELL_CONTENT.NOTHING
                    : CELL_CONTENT.OBSTACLE;

                columns.push(cellContent);
            }

            this.matrix.push(columns);
        }
    }
}

export default Grid;
