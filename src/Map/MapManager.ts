import { Tilemaps } from 'phaser';
import Grid from '@game/Map/Grid';
import {
    AbsolutePosition,
    Callback,
    Size,
    TargetPosition,
    TilePosition
} from '@game/Types';
import PathFinder from '@game/Map/PathFinder';

class MapManager {
    private readonly map: Tilemaps.Tilemap;
    private readonly baseLayer: Tilemaps.TilemapLayer;
    private readonly grid: Grid;
    private readonly pathFinder: PathFinder;

    public constructor(map: Tilemaps.Tilemap, baseLayer: Tilemaps.TilemapLayer) {
        this.map = map;
        this.baseLayer = baseLayer;
        this.grid = new Grid(map, baseLayer);
        this.pathFinder = new PathFinder();
    }

    public getSizeInPixels(): Size {
        return {
            width: this.map.widthInPixels,
            height: this.map.heightInPixels,
        };
    }

    public getSizeInTiles(): Size {
        return {
            width: this.map.width,
            height: this.map.height,
        };
    }

    public getGrid(): Grid {
        return this.grid;
    }

    public getNormalizedWorldXY(worldX: number, worldY: number): AbsolutePosition {
        return this.getWorldXYFromTilePosition(
            this.getTilePositionFromWorldXY(Math.ceil(worldX), Math.ceil(worldY))
        );
    }

    public getWorldXYFromTilePosition(position: TilePosition): AbsolutePosition {
        return this.baseLayer.tileToWorldXY(position.x, position.y) as AbsolutePosition;
    }

    public getTilePositionFromWorldXY(worldX: number, worldY: number): TilePosition {
        return this.baseLayer.worldToTileXY(worldX, worldY) as TilePosition;
    }

    public findPath(from: TilePosition, to: TilePosition, freeTarget: boolean): TargetPosition[] | null {
        let gridCallback: Callback = () => {};

        if (freeTarget) {
            gridCallback = this.grid.freePosition(to);
        }

        const path = this.pathFinder.findPath(this.grid.getGrid(), from, to);
        gridCallback();

        return path;
    }
}

export default MapManager;
