import { GameObjects } from 'phaser';
import { Action, ActionType, GameScene } from '@game/Types';
import config from '@game/Config';
import { DEPTH_MARKUP_AVAILABLE_ACTIONS } from '@game/Constants';
import EventEmitter from '@game/Events/EventEmitter';

class MapMarkup {
    private scene: GameScene;
    private eventEmitter: EventEmitter;
    private availableActions: GameObjects.Rectangle[] = [];

    public constructor(scene: GameScene) {
        this.scene = scene;
        this.eventEmitter = this.scene.getEventEmitter();

        this.initEvents();
    }

    private initEvents(): void {
        this.eventEmitter.onMapMarkupShowActions(this.showAvailableActions.bind(this));
        this.eventEmitter.onMapMarkupHideActions(this.hideAvailableActions.bind(this));
    }

    private showAvailableActions(actions: Action[]): void {
        const colorMap = {
            // [ActionType.SELF]: 0xbbbbbb,
            [ActionType.SELF]: 0xf2ce02,
            [ActionType.MOVE]: 0x209c05,
            [ActionType.ATTACK]: 0xff0a0a,
            [ActionType.INTERACT]: 0xf2ce02,
        };

        for (let i = 0; i < actions.length; i++) {
            const action = actions[i];
            this.createRectangle(action.target.x, action.target.y, colorMap[action.type]);
        }
    }

    private hideAvailableActions(): void {
        this.availableActions.forEach(rectangle => rectangle.destroy());
    }

    private createRectangle(x: number, y: number, color: number): void {
        const rectangle = this.scene.add.rectangle(
            x * config.gridCellSize + (config.gridCellSize / 2),
            y * config.gridCellSize + (config.gridCellSize / 2),
            config.gridCellSize - 2,
            config.gridCellSize - 2,
            color,
            0.1
        );

        rectangle
            .setDepth(DEPTH_MARKUP_AVAILABLE_ACTIONS)
            .setStrokeStyle(1, color, 0.4)
            .setInteractive()
            .on('pointerover', () => rectangle.setAlpha(0.7))
            .on('pointerout', () => rectangle.clearAlpha());

        this.availableActions.push(rectangle);
    }
}

export default MapMarkup;
