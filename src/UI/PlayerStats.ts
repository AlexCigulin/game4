import { GameObjects, Scene } from 'phaser';
import { DEPTH_UI } from '@game/Constants';
import config from '@game/Config';
import Character from '@game/Models/Characters/Character';
import EventEmitter from '@game/Events/EventEmitter';
import { PlayerUIStats } from '@game/Types';

type StatBarResult = {
    text: GameObjects.Text,
    bar1: GameObjects.Rectangle,
    bar2: GameObjects.Rectangle,
};

class PlayerStats {
    private readonly scene: Scene;
    private readonly eventEmitter: EventEmitter;

    public constructor(scene: Scene, eventEmitter: EventEmitter) {
        this.scene = scene;
        this.eventEmitter = eventEmitter;

        this.init();
    }

    private init(): void {
        this.initApBar();
        this.initHpBar();
    }

    private initApBar(): void {
        const { text, bar1, bar2 } = this.drawStatBar('null', 100);

        this.scene.add.container(
            config.width as number - (bar1.width / 2) - 8,
            config.height as number - (bar1.height / 2) - 32,
            [ bar1, bar2, text ]
        );

        this.eventEmitter.onCharacterAttributesUpdated((character: Character, attributes: PlayerUIStats) => {
            if (character.isPlayer() && attributes.ap !== undefined) {
                text.setText(
                    attributes.ap.balance > 0
                        ? 'AP ' + attributes.ap.balance + '/' + attributes.ap.reserve
                        : 'Waiting'
                );
                this.updateBar(bar2, bar1.width / attributes.ap.reserve * attributes.ap.balance);
            }
        });
    }

    private initHpBar(): void {
        const { text, bar1, bar2 } = this.drawStatBar('null', 100);

        this.scene.add.container(
            config.width as number - (bar1.width / 2) - 8,
            config.height as number - (bar1.height / 2) - 8,
            [ bar1, bar2, text ]
        );

        this.eventEmitter.onCharacterAttributesUpdated((character: Character, attributes: PlayerUIStats) => {
            if (character.isPlayer() && attributes.hp !== undefined) {
                text.setText('HP ' + attributes.hp + '%')
                this.updateBar(bar2, bar1.width / 100 * attributes.hp);
            }
        });
    }

    private drawStatBar(initialText: string, initialValue: number): StatBarResult {
        const rectangle1 = this.scene.add.rectangle(0, 0, 120, 16, 0x000000, 0.2);

        rectangle1
            .setDepth(DEPTH_UI + 1)
            .setOrigin(0.5)
            .setStrokeStyle(1, 0x49453a, 1);

        const rectangle2 = this.scene.add.rectangle(0, 0, rectangle1.width, rectangle1.height, 0x49453a, 1);

        rectangle2
            .setDepth(DEPTH_UI + 2)
            .setOrigin(0.5);

        const text = this.scene.add.text(0, 0, initialText);

        text
            .setDepth(DEPTH_UI + 3)
            .setOrigin(0.5)
            .setStyle({
                fontFamily: 'Verdana',
                fontSize: '10px',
                color: '#ddd0af',
                stroke: 'black',
                strokeThickness: 2,
            });

        rectangle2.width = rectangle1.width / 100 * initialValue;

        return {
            text: text,
            bar1: rectangle1,
            bar2: rectangle2,
        };
    }

    private updateBar(bar: GameObjects.Rectangle, newValue: number): void {
        this.scene.tweens.add({
            targets: bar,
            width: newValue,
            ease: 'Linear',
            duration: 300,
        });
    }
}

export default PlayerStats;
