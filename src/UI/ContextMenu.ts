import config from '@game/Config';
import { GameObjects, Scene } from 'phaser';
import { Callback, ContextMenuConfig, ContextMenuItem } from '@game/Types';
import EventEmitter from '@game/Events/EventEmitter';

class ContextMenu {
    private readonly screenWidth = config.width as number;
    private readonly screenHeight = config.height as number;
    private readonly menuPadding = 8;
    private readonly menuItemHeight = 32;
    private readonly verticalMenuWidth = 176;
    private readonly horizontalMenuHeight = this.menuItemHeight + (this.menuPadding * 2);
    private readonly verticalMenuItemWidth = this.verticalMenuWidth - (this.menuPadding * 2);
    private readonly horizontalMenuItemWidth = 32;

    private readonly scene: Scene;
    private menuContainer!: GameObjects.Container;

    public constructor(scene: Scene, eventEmitter: EventEmitter) {
        this.scene = scene;

        eventEmitter.onShowContextMenu(this.showMenu.bind(this));
    }

    private showMenu(config: ContextMenuConfig): void {
        config = Object.assign({}, config);
        config.items = [ ...config.items ];

        this.menuContainer = this.scene.add.container(this.screenWidth / 2, this.screenHeight / 2);

        if (config.showCancel) {
            config.items.push({
                title: this.hasVerticalLayout(config) ? 'Cancel' : '✕',
                onClick: () => {
                    config.onCancel && config.onCancel();
                }
            });
        }

        this.drawBackdrop(config.onCancel);
        this.drawMenuBody(config);

        config.items.forEach((item: ContextMenuItem, index: number) => {
            this.drawMenuItem(config, index);
        });
    }

    private hideMenu(): void {
        this.menuContainer.destroy();
    }

    private drawBackdrop(onCancel?: Callback): void {
        const backdrop = this.scene.add.rectangle(0, 0, this.screenWidth, this.screenHeight, 0x000000, 0);
        backdrop
            .setOrigin(0.5)
            .setInteractive()
            .on('pointerdown', () => {
                this.hideMenu();
                onCancel && onCancel();
            })

        this.menuContainer.add(backdrop);
    }

    private drawMenuBody(config: ContextMenuConfig): void {
        const menuWidth = this.hasVerticalLayout(config)
            ? this.verticalMenuWidth
            : (config.items.length * this.horizontalMenuItemWidth) + ((config.items.length * this.menuPadding) + this.menuPadding)

        const menuHeight = this.hasVerticalLayout(config)
            ? (config.items.length * this.menuItemHeight) + ((config.items.length * this.menuPadding) + this.menuPadding)
            : this.horizontalMenuHeight;

        const menu = this.scene.add.rectangle(0, 0, menuWidth, menuHeight, 0x000000, 0.9);

        menu
            .setOrigin(0.5)
            .setStrokeStyle(1, 0x49453a, 1);

        this.menuContainer.add(menu);
    }

    private drawMenuItem(config: ContextMenuConfig, index: number): void {
        const item = config.items[index];
        let itemContainer: GameObjects.Container;

        if (this.hasVerticalLayout(config)) {
            itemContainer = this.scene.add.container(0, index * 40 - (20 * config.items.length) + 20);
        } else {
            itemContainer = this.scene.add.container(index * 40 - (20 * config.items.length) + 20, 0);
        }

        const itemBody = this.scene.add.rectangle(
            0,
            0,
            this.hasVerticalLayout(config) ? this.verticalMenuItemWidth : this.horizontalMenuItemWidth,
            this.menuItemHeight,
            0x000000,
            0
        );
        const itemText = this.scene.add.text(0, 0, item.title);

        itemBody
            .setOrigin(0.5)
            .setStrokeStyle(1, 0x49453a, 1)
            .setInteractive();

        itemText
            .setOrigin(0.5)
            .setStyle({
                fontFamily: 'Verdana',
                fontSize: '12px',
                color: '#ddd0af',
                stroke: 'black',
                strokeThickness: 2,
            });

        itemContainer.add(itemBody);
        itemContainer.add(itemText);
        this.menuContainer.add(itemContainer);

        itemBody
            .on('pointerover', () => itemBody.setFillStyle(0x49453a, 0.15))
            .on('pointerout', () => itemBody.setFillStyle(0x000000, 0))
            .on('pointerdown', () => {
                this.hideMenu();
                item.onClick();
            });
    }

    private hasVerticalLayout(config: ContextMenuConfig): boolean {
        return config.layout === undefined || config.layout === 'vertical';
    }
}

export default ContextMenu;
