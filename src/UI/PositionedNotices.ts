import { Scene } from 'phaser';
import { AbsolutePosition, NoticeType } from '@game/Types';
import { DEPTH_MARKUP_NOTICE } from '@game/Constants';
import config from '@game/Config';
import EventEmitter from '@game/Events/EventEmitter';

class PositionedNotices {
    private readonly scene: Scene;

    public constructor(scene: Scene, eventEmitter: EventEmitter) {
        this.scene = scene;

        eventEmitter.onShowPositionedNotice(this.showNotice.bind(this));
    }

    private showNotice(position: AbsolutePosition, text: string, type?: NoticeType): void {
        const colorMap = {
            [NoticeType.NEUTRAL]: '#a49a82',
            [NoticeType.GOOD]: '#209c05',
            [NoticeType.BAD]: '#ff0a0a',
        };

        const textObject = this.scene.add.text(
            position.x,
            position.y + (config.gridCellSize / 2),
            text.toUpperCase()
        );

        textObject
            .setDepth(DEPTH_MARKUP_NOTICE)
            .setOrigin(0.5)
            .setStyle({
                fontFamily: 'Verdana',
                fontSize: '10px',
                color: colorMap[type || NoticeType.NEUTRAL],
                stroke: 'black',
                strokeThickness: 2,
            });

        this.scene.time.delayedCall(500, () => textObject.destroy());
    }
}

export default PositionedNotices;
