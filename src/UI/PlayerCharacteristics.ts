import { GameObjects, Scene } from 'phaser';
import EventEmitter from '@game/Events/EventEmitter';
import Player from '@game/Models/Characters/Player';
import { Callback } from '@game/Types';
import config from '@game/Config';

type Align = 'left' | 'right';

class PlayerCharacteristics {
    private readonly screenWidth = config.width as number;
    private readonly screenHeight = config.height as number;
    private readonly padding = 8;

    private readonly scene: Scene;
    private container!: GameObjects.Container;

    public constructor(scene: Scene, eventEmitter: EventEmitter) {
        this.scene = scene;

        eventEmitter.onOpenPlayerCharacteristics(this.openPlayerCharacteristics.bind(this));
    }

    private openPlayerCharacteristics(player: Player, onClose: Callback): void {
        onClose();
        // this.container = this.scene.add.container(this.screenWidth / 2, this.screenHeight / 2);
        //
        // this.drawBackdrop(onClose);
        // this.drawBody(player, onClose);
    }

    // private closePlayerCharacteristics(): void {
    //     this.container.destroy();
    // }
    //
    // private drawBackdrop(onClose: Callback): void {
    //     const backdrop = this.scene.add.rectangle(0, 0, this.screenWidth, this.screenHeight, 0x000000, 0);
    //     backdrop
    //         .setOrigin(0.5)
    //         .setInteractive()
    //         .on('pointerdown', () => {
    //             this.closePlayerCharacteristics();
    //             onClose();
    //         })
    //
    //     this.container.add(backdrop);
    // }
    //
    // private drawBody(player: Player, onClose: Callback): void {
    //     const attributes = player.getAttributes();
    //     const body = this.scene.add.rectangle(0, 0, 432, 248, 0x000000, 0.9);
    //
    //     body
    //         .setOrigin(0.5)
    //         .setStrokeStyle(1, 0x49453a, 1);
    //
    //     this.container.add(body);
    //
    //     this.addText('General', 'left', 0, 1, true);
    //     this.addText('Hit Points', 'left', 1, 1);
    //     this.addText(`${attributes.getHitPoints()}/${attributes.getInitialHitPoints()} (${attributes.getHitPointsAsPercent()}%)`, 'right', 1, 1);
    //     this.addText('Experience', 'left', 2, 1);
    //     this.addText('' + attributes.getExperience(), 'right', 2, 1);
    //     this.addText('Level', 'left', 3, 1);
    //     this.addText('1', 'right', 3, 1);
    //
    //     this.addText('Attributes', 'left', 5, 1, true);
    //     this.addText('Strength', 'left', 6, 1);
    //     this.addText('' + attributes.getStrength(), 'right', 6, 1);
    //     this.addText('Agility', 'left', 7, 1);
    //     this.addText('' + attributes.getAgility(), 'right', 7, 1);
    //     this.addText('Mind', 'left', 8, 1);
    //     this.addText('' + attributes.getMind(), 'right', 8, 1);
    //     this.addText('Health', 'left', 9, 1);
    //     this.addText('' + attributes.getHealth(), 'right', 9, 1);
    //     this.addText('Luck', 'left', 10, 1);
    //     this.addText('' + attributes.getLuck(), 'right', 10, 1);
    //     this.addText('Move Points', 'left', 11, 1);
    //     this.addText('' + attributes.getMovePoints(), 'right', 11, 1);
    //
    //     this.addText('Melee Combat', 'left', 0, 2, true);
    //     this.addText('Attack Chance', 'left', 1, 2);
    //     this.addText('~' + calculateMeleeAttackChance(player) + '%', 'right', 1, 2);
    //     this.addText('Defence Chance', 'left', 2, 2);
    //     this.addText('~' + calculateMeleeDefenceChance(player) + '%', 'right', 2, 2);
    //     const { min, max } = calculatePossibleDamageRange(player);
    //     this.addText('Damage', 'left', 3, 2);
    //     this.addText(`${min}-${max}`, 'right', 3, 2);
    //
    //     this.addText('Range Combat', 'left', 5, 2, true);
    //     this.addText('Attack Chance', 'left', 6, 2);
    //     this.addText('~0%', 'right', 6, 2);
    //     this.addText('Defence Chance', 'left', 7, 2);
    //     this.addText('~10%', 'right', 7, 2);
    //     this.addText('Damage', 'left', 8, 2);
    //     this.addText(`2-3`, 'right', 8, 2);
    // }
    //
    // private addText(text: string, align: Align, lineNumber: number, column: number, title: boolean = false): void {
    //     const x = align === 'left' ? 8 : 0;
    //     const y = 8 + (lineNumber * 20);
    //     const textObject = this.scene.add.text(x - (432 / 2), y - (248 / 2), title ? text.toUpperCase() : text);
    //
    //     if (align === 'left') {
    //         textObject.setOrigin(0);
    //
    //         if (column === 2) {
    //             textObject.x += 216;
    //         }
    //     } else {
    //         textObject.setOrigin(1, 0);
    //
    //         if (column === 1) {
    //             textObject.x = 432 / 2 - 8 - (432 / 2);
    //         } else {
    //             textObject.x = 432 / 2 - 8;
    //         }
    //
    //     }
    //
    //     textObject
    //         .setStyle({
    //             fontFamily: 'Verdana',
    //             fontSize: '12px',//title ? '14px' : '12px',
    //             color: align === 'left' ? '#ddd0af' : '#988f78',
    //             stroke: 'black',
    //             strokeThickness: 2,
    //         });
    //
    //     if (title) {
    //         textObject.setColor('#cc966b');
    //     }
    //
    //     this.container.add(textObject);
    // }
}

export default PlayerCharacteristics;
