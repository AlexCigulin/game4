import Candle from '@game/Models/Decorative/Candle';

class Candles extends Candle {
    public constructor(scene: Phaser.Scene, x: number, y: number, tilePosiyionY: number) {
        super(scene, x, y, tilePosiyionY, 'candles');
        this.init(tilePosiyionY);
    }

    protected init(tilePosiyionY: number): void {
        super.init(tilePosiyionY);

        this.setScale(0.8);
        this.y -= 1;

        this.light.x -= 1;
        this.light.y += 2;
        this.light.intensity = 0.08;
        this.light.radius = 20;
    }
}

export default Candles;
