import { GameObjects, Math } from 'phaser';
import { DEPTH_MODEL } from '@game/Constants';
import config from '@game/Config';

class Candle extends GameObjects.Sprite {
    protected light!: GameObjects.PointLight;

    public constructor(scene: Phaser.Scene, x: number, y: number, tilePosiyionY: number, texture: string = 'candle') {
        super(scene, x + 2, y, texture, 0);
        this.init(tilePosiyionY);
    }

    protected init(tilePosiyionY: number): void {
        this.setDepth(DEPTH_MODEL + tilePosiyionY);
        this.setOrigin(0);
        this.setScale(0.8);
        this.scene.add.existing(this);

        this.light = this.scene.add.pointlight(
            this.x + (config.gridCellSize / 2),
            this.y + (config.gridCellSize / 2) - 4,
            Candle.getLightColor(),
            16,
            0.08
        );

        this.light.setDepth(DEPTH_MODEL + tilePosiyionY);

        this.scene.anims.create({
            key: 'default-' + this.texture.key,
            frameRate: 8,
            yoyo: true,
            frames: this.scene.anims.generateFrameNumbers(this.texture.key, { start: 0, end: 3 }),
            repeat: -1,
        });

        this.play('default-' + this.texture.key);

        this.scene.tweens.add({
            targets: this.light,
            radius: this.light.radius + 2,
            ease: 'Bounce',
            yoyo: true,
            repeat: -1,
            duration: 1000,
        });
    }

    public static getLightColor(): number {
        const colors = [
            0xff4400,
            0xff5500,
            0xff6600,
        ];

        return Math.RND.shuffle(colors)[0];
    }
}

export default Candle;
