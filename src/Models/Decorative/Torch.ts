import Candle from '@game/Models/Decorative/Candle';
import { DEPTH_TOP_LAYER } from '@game/Constants';

class Torch extends Candle {
    public constructor(scene: Phaser.Scene, x: number, y: number, tilePosiyionY: number) {
        super(scene, x, y, tilePosiyionY, 'torch');
        this.init(tilePosiyionY);
    }

    protected init(tilePosiyionY: number): void {
        super.init(tilePosiyionY);

        this.setScale(0.8);
        this.setDepth(DEPTH_TOP_LAYER - 1);

        this.light.x -= 2;
    }
}

export default Torch;
