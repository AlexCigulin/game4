import { GameObjects, Math } from 'phaser';
import { DEPTH_MODEL } from '@game/Constants';
import config from '@game/Config';

class Lamp extends GameObjects.Sprite {
    public constructor(scene: Phaser.Scene, x: number, y: number, tilePosiyionY: number) {
        super(scene, x + 6, y - 6, 'lamp');
        this.init(tilePosiyionY);
    }

    private init(tilePosiyionY: number): void {
        this.setDepth(DEPTH_MODEL + tilePosiyionY);
        this.setOrigin(0);
        this.setScale(0.3);
        this.scene.add.existing(this);

        const light = this.scene.add.pointlight(
            this.x + (config.gridCellSize / 2) - 5,
            this.y + (config.gridCellSize / 2) - 5,
            Lamp.getLightColor(),
            10,
            0.18
        );

        light.setDepth(DEPTH_MODEL + tilePosiyionY);

        this.scene.tweens.add({
            targets: light,
            intensity: light.intensity + 0.04,
            ease: 'Linear',
            yoyo: true,
            repeat: -1,
            duration: 800,
        });
    }

    private static getLightColor(): number {
        const colors = [
            0xeba4e9,
            0xb6a4eb,
        ];

        return Math.RND.shuffle(colors)[0];
    }
}

export default Lamp;
