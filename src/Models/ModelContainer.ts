import { GameScene, ModelInterface, ModelType } from '@game/Types';
import Player from '@game/Models/Characters/Player';
import Enemy from '@game/Models/Characters/Enemy';

type ModelMap = {
    [id: string]: ModelInterface;
};

class ModelContainer {
    private scene: GameScene;
    private modelMap: ModelMap = {};

    public constructor(scene: GameScene) {
        this.scene = scene;
    }

    public addModel(model: ModelInterface): this {
        if (this.hasModel(model.getId())) {
            throw new Error('Model has already been added: ' + model.getId());
        }

        this.modelMap[model.getId()] = model;

        const tilePosition = this.scene.getMapManager().getTilePositionFromWorldXY(model.x, model.y);

        this.scene.getMapManager().getGrid().addModel(model, tilePosition);

        return this;
    }

    public removeModel(id: string): this {
        if (!this.hasModel(id)) {
            throw new Error('Model not found: ' + id);
        }

        const model = this.modelMap[id];
        this.scene.getMapManager().getGrid().removeModel(model);
        delete this.modelMap[id];

        return this;
    }

    public getModel(id: string): ModelInterface {
        if (!this.hasModel(id)) {
            throw new Error('Model not found: ' + id);
        }

        return this.modelMap[id];
    }

    public getPlayer(): Player {
        const models = this.getModelsByType(ModelType.PLAYER);

        if (!models.length) {
            throw new Error('Cannot find player');
        }

        return models[0] as Player;
    }

    public getEnemies(): Enemy[] {
        return this.getModelsByType(ModelType.ENEMY) as Enemy[];
    }

    private getModelsByType(type: ModelType): ModelInterface[] {
        const models: ModelInterface[] = [];

        for (const [id, model] of Object.entries(this.modelMap)) {
            if (model.getType() === type) {
                models.push(model);
            }
        }

        return models;
    }

    public hasModel(id: string): boolean {
        return this.modelMap[id] !== undefined;
    }
}

export default ModelContainer;
