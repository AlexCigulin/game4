import { Math } from 'phaser';
import { DEPTH_MODEL } from '@game/Constants';
import config from '@game/Config';
import Loot from '@game/Models/Loot/Loot';
import { GameScene, LootSoundConfid } from '@game/Types';
import Character from '@game/Models/Characters/Character';

class Potion extends Loot {
    protected displayedName: string = 'Potion';

    public constructor(scene: GameScene, x: number, y: number, tilePosiyionY: number) {
        super(scene, x + 3, y + 2, tilePosiyionY, 'potion', 0);
        this.id = Math.RND.uuid();
    }

    public getSoundConfig(): LootSoundConfid {
        return {
            onTake: 'potion_take',
            onUse: 'potion_use',
        };
    }

    public useBy(character: Character) {
        super.useBy(character);
        character.getAttributes().increaseHealthPoints(5); // FIXME
    }

    protected init(tilePosiyionY: number): void {
        super.init(tilePosiyionY);

        this.sprite.setScale(0.35);
        this.scene.add.existing(this.sprite);

        const light = this.scene.add.pointlight(
            this.x + (config.gridCellSize / 2) - 3,
            this.y + (config.gridCellSize / 2) - 2,
            0xffff00,
            10,
            0.03
        );

        light.setDepth(DEPTH_MODEL + tilePosiyionY);

        this.container.add(light);
    }
}

export default Potion;
