import { GameObjects } from 'phaser';
import { DEPTH_MODEL } from '@game/Constants';
import { GameScene, LootSoundConfid, ModelInterface, ModelType } from '@game/Types';
import LootSoundFXProxy from '@game/Sounds/LootSoundFXProxy';
import Character from '@game/Models/Characters/Character';

abstract class Loot implements ModelInterface {
    public x: number;
    public y: number;
    protected id!: string;
    protected scene: GameScene;
    protected sound: LootSoundFXProxy;
    protected container: GameObjects.Container;
    protected sprite: GameObjects.Sprite;
    protected displayedName!: string;

    protected constructor(scene: GameScene, x: number, y: number, tilePosiyionY: number, texture: string, frame: number) {
        this.scene = scene;
        this.container = this.scene.add.container(x, y);
        this.sprite = this.scene.add.sprite(x, y, texture, frame);
        this.sound = this.scene.getLootSoundFXProxy();

        this.x = x;
        this.y = y;
        this.container.add(this.sprite);

        this.init(tilePosiyionY);
    }

    public getId(): string {
        return this.id;
    }

    public getDisplayedName(): string {
        return this.displayedName;
    }

    public getType(): ModelType {
        return ModelType.LOOT;
    }

    public isCharacter(): boolean {
        return false;
    }

    public isPlayer(): boolean {
        return false;
    }

    public isEnemy(): boolean {
        return false;
    }

    public getSoundConfig(): LootSoundConfid {
        return {
            onTake: 'loot_take',
            onUse: 'loot_use',
        }
    }

    public takeBy(character: Character): void {
        character.getAttributes().getBackpack().addItem(this);
        this.sound.playOnTake(this);
        this.container.destroy();
        this.scene.getModelContainer().removeModel(this.getId());
    }

    public useBy(character: Character): void {
        this.sound.playOnUse(this);
    }

    protected init(tilePosiyionY: number): void {
        this.sprite.setDepth(DEPTH_MODEL + tilePosiyionY);
        this.sprite.setOrigin(0);
    }
}

export default Loot;
