import { Animations, GameObjects } from 'phaser';
import {
    Callback,
    CharacterAnimationConfig, CharacterSoundConfig,
    CharacterState,
    Direction,
    GameScene,
    ModelInterface,
    ModelType,
} from '@game/Types';
import { DEPTH_MODEL } from '@game/Constants';
import Attributes from '@game/Core/RPGSystem/Character/Attributes';
import Equipment from '@game/Core/RPGSystem/Character/Equipment/Equipment';
import CharacterSoundFXProxy from '@game/Sounds/CharacterSoundFXProxy';

abstract class Character extends GameObjects.Container implements ModelInterface {
    protected sprite: GameObjects.Sprite;
    protected sound: CharacterSoundFXProxy;
    protected readonly id: string;
    protected displayedName: string;
    protected direction: Direction;
    protected currentState: CharacterState;
    protected attributes!: Attributes;
    protected holdsPosition: boolean;

    protected constructor(
        id: string,
        scene: GameScene,
        x: number,
        y: number,
        texture: string,
        frame?: string | number
    ) {
        super(scene, x, y);

        this.sprite = new GameObjects.Sprite(scene, 0, 0, texture, frame);
        this.sound = this.getScene().getCharacterSoundFXProxy();

        this.id = id;
        this.displayedName = id;
        this.direction = Direction.S;
        this.currentState = CharacterState.IDLE;
        this.holdsPosition = false;

        this.x += 8;
        this.y += 8;
        this.width = 16;
        this.height = 16;

        this.add(this.sprite);

        this.scene.add.existing(this);
        this.scene.add.existing(this.sprite);
        this.scene.physics.add.existing(this);

        this.updateDepth(y);
        this.initAnimations();
    }

    public abstract getType(): ModelType;

    public getScene(): GameScene {
        return this.scene as GameScene;
    }

    public isCharacter(): boolean {
        return true;
    }

    public isPlayer(): boolean {
        return this.getType() === ModelType.PLAYER;
    }

    public isEnemy(): boolean {
        return this.getType() === ModelType.ENEMY;
    }

    public abstract getAnimationConfig(): CharacterAnimationConfig;

    public getSoundConfig(): CharacterSoundConfig {
        return {
            walk: 'walk_160bpm',
            attackMelee: 'attack_melee',
            attackImpact: 'attack_impact',
            die: 'die',
        };
    }

    public getId(): string {
        return this.id;
    }

    public setDisplayedName(name: string): this {
        this.displayedName = name;
        return this;
    }

    public getDisplayedName(): string {
        return this.displayedName;
    }

    public updateDepth(yAxisIndex: number): this {
        this.setDepth(DEPTH_MODEL + yAxisIndex);
        return this;
    }

    public isIdle(): boolean {
        return this.currentState === CharacterState.IDLE;
    }

    public setIdle(): this {
        this.currentState = CharacterState.IDLE;
        return this;
    }

    public setBusy(): this {
        this.currentState = CharacterState.BUSY;
        return this;
    }

    public setDead(): this {
        this.currentState = CharacterState.DEAD;
        return this;
    }

    public isBusy(): boolean {
        return this.currentState === CharacterState.BUSY;
    }

    public isDead(): boolean {
        return this.currentState === CharacterState.DEAD || this.getAttributes().getHealthPoints() === 0;
    }

    public isHoldingPosition(): boolean {
        return this.holdsPosition;
    }

    public setHoldsPosition(holdsPosition: boolean): this {
        if (holdsPosition) {
            this.sound.playHoldPosition();
        }

        this.holdsPosition = holdsPosition;
        return this;
    }

    public updateDirection(direction: Direction): this {
        this.direction = direction;
        return this;
    }

    public getDirection(): Direction {
        return this.direction;
    }

    public getAttributes(): Attributes {
        return this.attributes;
    }

    public getEquipment(): Equipment {
        return this.attributes.getEquipment();
    }

    public playIdle(): this {
        this.sprite.play(this.sprite.texture.key + '-idle-' + this.direction);
        return this;
    }

    public playActivated(): this {
        this.sound.playActivated(this);
        return this;
    }

    public playWalk(): Callback {
        this.sprite.play(this.sprite.texture.key + '-walk-' + this.direction);
        return this.sound.playWalk(this);
    }

    public playAttack(onComplete?: Callback): this {
        this.sprite.play(this.sprite.texture.key + '-attack-' + this.direction);
        this.sound.playMeleeAttack(this);

        if (onComplete) {
            this.sprite.once(Animations.Events.ANIMATION_COMPLETE, (animation: Animations.Animation) => {
                if (animation.key.includes(this.sprite.texture.key + '-attack')) {
                    onComplete();
                }
            });
        }

        return this;
    }

    public playAttackImpact(): this {
        this.sound.playAttackImpact(this);
        return this;
    }

    public playDefend(onComplete?: () => void): void {
        this.sprite.play(this.sprite.texture.key + '-defend-' + this.direction);

        if (onComplete) {
            this.sprite.once(Animations.Events.ANIMATION_COMPLETE, (animation: Animations.Animation) => {
                if (animation.key.includes(this.sprite.texture.key + '-defend')) {
                    onComplete();
                }
            });
        }
    }

    public playDie(onComplete?: () => void): void {
        this.sprite.play(this.sprite.texture.key + '-die-' + this.direction);
        this.sound.playDie(this);

        if (onComplete) {
            this.sprite.once(Animations.Events.ANIMATION_COMPLETE, (animation: Animations.Animation) => {
                if (animation.key.includes(this.sprite.texture.key + '-die')) {
                    onComplete();
                }
            });
        }

        this.scene.time.delayedCall(1000, () => {
            this.scene.tweens.add({
                targets: this,
                alpha: 0,
                duration: 1000,
                onComplete: () => this.destroy(),
            });
        });
    }

    protected initAnimations(): void {
        // Should be overriden
    }

    protected addAnimation(
        key: string,
        direction: Direction,
        startFrame: number,
        framesCount: number,
        frameRate: number,
        repeatForever: boolean = false
    ): void {
        this.scene.anims.create({
            key: this.sprite.texture.key + '-' + key + '-' + direction,
            frameRate: frameRate,
            frames: this.scene.anims.generateFrameNumbers(
                this.sprite.texture.key, { start: startFrame, end: startFrame + framesCount - 1 }
            ),
            repeat: repeatForever ? -1 : 0,
        });
    }
}

export default Character;
