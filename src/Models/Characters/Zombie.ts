import Enemy from '@game/Models/Characters/Enemy';
import { CharacterAnimationConfig, CharacterSoundConfig, Direction, GameScene } from '@game/Types';
import Attributes from '@game/Core/RPGSystem/Character/Attributes';
import Equipment from '@game/Core/RPGSystem/Character/Equipment/Equipment';

class Zombie extends Enemy {
    public constructor(id: string, scene: GameScene, x: number, y: number) {
        super(id, scene, x, y, 'zombie', 216);

        this.sprite.setScale(0.55);
        this.sprite.setDisplayOrigin(64, 91);
        this.playIdle();

        this.attributes = new Attributes(
            this,
            {
                actionPoints: 2,
                healthPoints: 12,
                movePoints: 3,
                strength: 4,
                agility: 2,
            },
            new Equipment()
        );
    }

    public getAnimationConfig(): CharacterAnimationConfig {
        return {
            idleFrameRate: 4,
            walkFrameRate: 10,
            walkDuration: 800,
            attackFrameRate: 16,
            defendFrameRate: 8,
            dieFrameRate: 16,
        };
    }

    public getSoundConfig(): CharacterSoundConfig {
        return Object.assign(
            super.getSoundConfig(),
            {
                walk: 'walk_130bpm',
                activated: 'activated_zombie',
                die: 'die_zombie',
                attackImpact: 'attack_impact_zombie',
            }
        );
    }

    protected initAnimations(): void {
        const config = this.getAnimationConfig();

        this.addAnimation('idle', Direction.N, 72, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.NE, 108, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.E, 144, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.SE, 180, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.S, 216, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.SW, 252, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.W, 0, 4, config.idleFrameRate, true);
        this.addAnimation('idle', Direction.NW, 36, 4, config.idleFrameRate, true);

        this.addAnimation('walk', Direction.N, 76, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.NE, 112, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.E, 148, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.SE, 184, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.S, 220, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.SW, 256, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.W, 4, 8, config.walkFrameRate, true);
        this.addAnimation('walk', Direction.NW, 40, 8, config.walkFrameRate, true);

        this.addAnimation('attack', Direction.N, 88, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.NE, 124, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.E, 160, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.SE, 196, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.S, 232, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.SW, 268, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.W, 16, 2, config.attackFrameRate);
        this.addAnimation('attack', Direction.NW, 52, 2, config.attackFrameRate);

        this.addAnimation('defend', Direction.N, 92, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.NE, 128, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.E, 164, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.SE, 200, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.S, 236, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.SW, 272, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.W, 20, 2, config.defendFrameRate);
        this.addAnimation('defend', Direction.NW, 56, 2, config.defendFrameRate);

        this.addAnimation('die', Direction.N, 100, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.NE, 136, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.E, 172, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.SE, 208, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.S, 244, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.SW, 280, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.W, 28, 8, config.dieFrameRate);
        this.addAnimation('die', Direction.NW, 64, 8, config.dieFrameRate);
    }
}

export default Zombie;
