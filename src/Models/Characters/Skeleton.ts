// import Enemy from '@game/Models/Characters/Enemy';
// import { CharacterAnimationConfig, Direction, GameScene } from '@game/Types';
// import Attributes from '@game/Core/RPGSystem/Character/Attributes';
// import Equipment from '@game/Core/RPGSystem/Character/Equipment/Equipment';
//
// class Skeleton extends Enemy {
//     public constructor(id: string, scene: GameScene, x: number, y: number) {
//         super(id, scene, x, y, 'skeleton', 216);
//
//         this.sprite.setScale(0.47);
//         this.sprite.setDisplayOrigin(62, 86);
//         this.playIdle();
//
//         this.attributes = new Attributes(this, { strength: 2, agility: 2, mind: 1, health: 1 }, new Equipment());
//     }
//
//     public getAnimationConfig(): CharacterAnimationConfig {
//         return {
//             idleFrameRate: 4,
//             walkFrameRate: 10,
//             walkDuration: 1000,
//             attackFrameRate: 16,
//             defendFrameRate: 8,
//             dieFrameRate: 16,
//         };
//     }
//
//     protected initAnimations(): void {
//         const config = this.getAnimationConfig();
//
//         this.addAnimation('idle', Direction.N, 64, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.NE, 96, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.E, 128, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.SE, 160, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.S, 192, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.SW, 224, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.W, 0, 4, config.idleFrameRate, true);
//         this.addAnimation('idle', Direction.NW, 32, 4, config.idleFrameRate, true);
//
//         this.addAnimation('walk', Direction.N, 68, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.NE, 100, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.E, 132, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.SE, 164, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.S, 196, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.SW, 228, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.W, 4, 8, config.walkFrameRate, true);
//         this.addAnimation('walk', Direction.NW, 36, 8, config.walkFrameRate, true);
//
//         this.addAnimation('attack', Direction.N, 76, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.NE, 108, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.E, 140, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.SE, 172, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.S, 204, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.SW, 236, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.W, 12, 4, config.attackFrameRate);
//         this.addAnimation('attack', Direction.NW, 44, 4, config.attackFrameRate);
//
//         this.addAnimation('defend', Direction.N, 80, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.NE, 112, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.E, 144, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.SE, 176, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.S, 208, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.SW, 240, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.W, 16, 2, config.defendFrameRate);
//         this.addAnimation('defend', Direction.NW, 48, 2, config.defendFrameRate);
//
//         this.addAnimation('die', Direction.N, 86, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.NE, 118, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.E, 150, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.SE, 182, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.S, 214, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.SW, 246, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.W, 22, 6, config.dieFrameRate);
//         this.addAnimation('die', Direction.SW, 54, 6, config.dieFrameRate);
//     }
// }
//
// export default Skeleton;
