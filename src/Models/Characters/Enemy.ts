import config from '@game/Config';
import Character from '@game/Models/Characters/Character';
import { GameScene, ModelType, PlayerUIStats } from '@game/Types';
import { GameObjects } from 'phaser';

abstract class Enemy extends Character {
    protected hpContainer!: GameObjects.Container;

    protected constructor(id: string, scene: GameScene, x: number, y: number, texture: string, frame: number) {
        super(id, scene, x, y, texture, frame);

        this.initHPBar();
    }

    public getType(): ModelType {
        return ModelType.ENEMY;
    }

    public updateDepth(yAxisIndex: number): this {
        super.updateDepth(yAxisIndex);

        if (this.hpContainer) {
            this.hpContainer.setDepth(this.depth);
        }

        return this;
    }

    protected initHPBar(): void {
        const rect1 = this.scene.add.rectangle(0, 0, 12, 1, 0xff0a0a, 1);
        rect1.setOrigin(0.5);

        const rect2 = this.scene.add.rectangle(0, 0, 12, 1, 0x209c05, 1);
        rect2.setOrigin(0.5);

        this.hpContainer = this.scene.add.container(
            this.x + (config.gridCellSize / 2),
            this.y + (config.gridCellSize / 2),
            [ rect1, rect2 ]
        );
        this.hpContainer.setDepth(this.depth + 1);

        const eventEmitter = this.getScene().getEventEmitter();

        eventEmitter.onCharacterAttributesUpdated((character: Character, attributes: PlayerUIStats) => {
            if (character.getId() === this.getId() && attributes.hp !== undefined) {
                if (attributes.hp === 0 || attributes.hp === 100) {
                    rect1.setAlpha(0);
                    rect2.setAlpha(0);
                } else {
                    rect1.setAlpha(1);
                    rect2.setAlpha(1);
                    rect2.width = rect1.width / 100 * attributes.hp;
                }
            }
        });

        this.add(rect1);
        this.add(rect2);

        this.updateDepth(this.y);
    }
}

export default Enemy;
