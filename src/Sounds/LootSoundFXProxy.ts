import { GameScene } from '@game/Types';
import SoundFXProxy from '@game/Sounds/SoundFXProxy';
import Loot from '@game/Models/Loot/Loot';

class CharacterSoundFXProxy extends SoundFXProxy {
    public constructor(scene: GameScene) {
        super(scene);
        this.init();
    }

    public playOnTake(loot: Loot): void {
        const group = loot.getSoundConfig().onTake;

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.2 });
    }

    public playOnUse(loot: Loot): void {
        const group = loot.getSoundConfig().onUse;

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.2 });
    }

    protected init(): void {
        this.addSound('loot_take', 'loot_take1');
        this.addSound('loot_use', 'loot_use1');
        this.addSound('potion_take', 'potion_take1');
        this.addSound('potion_use', 'potion_use1');
    }
}

export default CharacterSoundFXProxy;
