import { Sound, Math } from 'phaser';
import { GameScene } from '@game/Types';

type SoundManager = Sound.NoAudioSoundManager | Sound.HTML5AudioSoundManager | Sound.WebAudioSoundManager;

type SoundMap = {
    [groupName: string]: SoundGroup;
};

type SoundGroup = {
    [name: string]: Sound.BaseSound;
};

abstract class SoundFXProxy {
    protected sound: SoundManager;
    protected map: SoundMap = {};

    protected constructor(scene: GameScene) {
        this.sound = scene.sound;
    }

    // Should be called from constructor
    protected abstract init(): void;

    protected addSound(group: string, name: string): void {
        if (this.map[group] === undefined) {
            this.map[group] = {};
        }

        this.map[group][name] = this.sound.add(name, { loop: false });
    }

    protected getRandomSoundFromGroup(group: string): Sound.BaseSound {
        const soundNames = Object.keys(this.map[group]);
        const randomName = Math.RND.pick([ ...soundNames ]);

        return this.map[group][randomName];
    }
}

export default SoundFXProxy;
