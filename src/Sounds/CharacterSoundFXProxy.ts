import { Callback, GameScene } from '@game/Types';
import Character from '@game/Models/Characters/Character';
import SoundFXProxy from '@game/Sounds/SoundFXProxy';

class CharacterSoundFXProxy extends SoundFXProxy {
    public constructor(scene: GameScene) {
        super(scene);
        this.init();
    }

    public playHoldPosition(): void {
        this.getRandomSoundFromGroup('hold_position').play({ volume: 0.3 });
    }

    public playActivated(character: Character): void {
        const group = character.getSoundConfig().activated || '';

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.2 });
    }

    public playWalk(character: Character): Callback {
        const group = character.getSoundConfig().walk || '';

        if (this.map[group] === undefined) {
            return () => {};
        }

        const sound = this.getRandomSoundFromGroup(group);
        sound.play({ volume: 0.3, loop: true });

        return () => {
            sound.stop();
        };
    }

    public playMeleeAttack(character: Character): void {
        const group = character.getSoundConfig().attackMelee || '';

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.3 });
    }

    public playAttackImpact(character: Character): void {
        const group = character.getSoundConfig().attackImpact || '';

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.2 });
    }

    public playDie(character: Character): void {
        const group = character.getSoundConfig().die || '';

        if (this.map[group] === undefined) {
            return;
        }

        this.getRandomSoundFromGroup(group).play({ volume: 0.3 });
    }

    protected init(): void {
        this.addSound('attack_impact', 'attack_impact1');
        this.addSound('attack_impact', 'attack_impact2');
        this.addSound('attack_impact_zombie', 'attack_impact_zombie1');
        this.addSound('attack_melee', 'attack_melee1');
        this.addSound('attack_melee', 'attack_melee2');
        this.addSound('attack_melee', 'attack_melee3');
        this.addSound('attack_melee', 'attack_melee4');
        this.addSound('die', 'die1');
        this.addSound('die_zombie', 'die_zombie1');
        this.addSound('hold_position', 'hold_position1');
        this.addSound('walk_160bpm', 'walk_160bpm1');
        this.addSound('walk_130bpm', 'walk_130bpm1');
        this.addSound('activated_zombie', 'activated_zombie1');
        this.addSound('activated_zombie', 'activated_zombie2');
    }
}

export default CharacterSoundFXProxy;
