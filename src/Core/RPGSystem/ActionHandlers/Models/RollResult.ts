enum RollResultType {
    SUCCESS,
    CRITICAL_SUCCESS,
    FAILURE,
    CRITICAL_FAILURE,
}

class RollResult {
    private readonly type: RollResultType;

    private constructor(type: RollResultType) {
        this.type = type;
    }

    public static success(): RollResult {
        return new this(RollResultType.SUCCESS);
    }

    public static criticalSuccess(): RollResult {
        return new this(RollResultType.CRITICAL_SUCCESS);
    }

    public static failure(): RollResult {
        return new this(RollResultType.FAILURE);
    }

    public static criticalFailure(): RollResult {
        return new this(RollResultType.CRITICAL_FAILURE);
    }

    public isSuccess(): boolean {
        return this.type === RollResultType.SUCCESS || this.type === RollResultType.CRITICAL_SUCCESS;
    }

    public isCriticalSuccess(): boolean {
        return this.type === RollResultType.CRITICAL_SUCCESS;
    }

    public isFailure(): boolean {
        return this.type === RollResultType.FAILURE || this.type === RollResultType.CRITICAL_FAILURE;
    }

    public isCriticalFailure(): boolean {
        return this.type === RollResultType.CRITICAL_FAILURE;
    }
}

export default RollResult;
