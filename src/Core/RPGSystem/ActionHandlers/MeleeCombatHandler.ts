import Character from '@game/Models/Characters/Character';
import { NoticeType } from '@game/Types';
import {
    calculateDamage,
    calculateMeleeAttackChance,
    calculateMeleeDefenceChance,
    rollResult
} from '@game/Core/RPGSystem/ActionHandlers/Helpers';
import RollResult from '@game/Core/RPGSystem/ActionHandlers/Models/RollResult';
import EventEmitter from '@game/Events/EventEmitter';

class MeleeCombatHandler {
    private eventEmitter: EventEmitter;

    public constructor(eventEmitter: EventEmitter) {
        this.eventEmitter = eventEmitter;
    }

    public handle(initiator: Character, target: Character): boolean {
        const attackChance = calculateMeleeAttackChance(initiator);
        const attackResult = rollResult(initiator, attackChance);

        if (attackResult.isFailure()) {
            this.showNotification(initiator, 'Failure');
            return false;
        }

        let defenceResult: RollResult | null = null;

        if (!attackResult.isCriticalSuccess()) {
            const defenceChance = calculateMeleeDefenceChance(target, initiator);
            defenceResult = rollResult(target, defenceChance);

            if (defenceResult.isSuccess()) {
                let defenceType = 'Dodged';

                if (target.getEquipment().hasShield()) {
                    defenceType = 'Blocked';
                } else if (target.getEquipment().hasWeapon()) {
                    defenceType = 'Parried';
                }

                this.showNotification(target, defenceType);
                return false;
            }
        }

        const damage = calculateDamage(initiator, target, attackResult, defenceResult);

        if (damage === 0) {
            this.showNotification(target, 'Blocked');
            return false;
        }

        target.getAttributes().decreaseHitPoints(damage);

        const notificationText = target.isDead() ? 'Killed' : '-' + damage + ' HP';
        this.showNotification(target, notificationText, NoticeType.BAD);

        return true;
    }

    private showNotification(target: Character, text: string, type?: NoticeType): void {
        this.eventEmitter.emitShowCharacterNotice(target, text, type);
    }
}

export default MeleeCombatHandler;
