import Phaser from 'phaser';
import Character from '@game/Models/Characters/Character';
import { CRITICAL_FAILURE_THRESHOLD, CRITICAL_SUCCESS_THRESHOLD } from '@game/Constants';
import RollResult from '@game/Core/RPGSystem/ActionHandlers/Models/RollResult';
import { Damage } from '@game/Types';

export function rollResult(resultTarget: Character, chance: number): RollResult {
    const result = Phaser.Math.RND.integerInRange(1, 100);

    if (CRITICAL_SUCCESS_THRESHOLD + resultTarget.getAttributes().getLuck() >= result) {
        return RollResult.criticalSuccess();
    }

    if (chance >= result) {
        return RollResult.success();
    }

    if (result >= CRITICAL_FAILURE_THRESHOLD) {
        return RollResult.criticalFailure();
    }

    return RollResult.failure();
}

export function calculateMeleeAttackChance(character: Character): number {
    let speedPenalty = 0;

    // TODO consider penalty for heavy (2 AP) attacks only
    // if (character.getEquipment().hasWeapon()) {
    //     speedPenalty = character.getEquipment().getWeapon().getSpeedPenalty() * 5;
    // }

    const meleeSkillModifier = character.getAttributes().getMeleeSkill() * 10;
    const agilityModifier = Math.floor(character.getAttributes().getAgility() * 7.5);
    const modifier = meleeSkillModifier > agilityModifier ? meleeSkillModifier : agilityModifier;

    return 45 + modifier - speedPenalty;
}

export function calculateMeleeDefenceChance(defendingCharacter: Character, attackInitiator?: Character): number {
    let chance = 0;

    if (defendingCharacter.isHoldingPosition()) {
        chance += 20;
    }

    if (attackInitiator && attackInitiator.getDirection() === defendingCharacter.getDirection()) {
        chance -= 20;
    }

    const blockingSkillModifier = defendingCharacter.getAttributes().getBlockingSkill() * 10;
    const agilityModifier = defendingCharacter.getAttributes().getAgility() * 5;
    const modifier = blockingSkillModifier > agilityModifier ? blockingSkillModifier : agilityModifier;

    chance += modifier;

    if (chance < 0) {
        chance = 0;
    }

    return chance;
}

export function calculateDamage(
    attackInitiator: Character,
    attackedTarget: Character,
    attackResult: RollResult,
    defenceResult: RollResult | null
): number {
    let targetArmorResistance = 0;

    if (attackedTarget.getEquipment().hasArmor()) {
        const { min, max } = attackedTarget.getEquipment().getArmor().getResistance();
        targetArmorResistance = Phaser.Math.RND.integerInRange(min, max);
    }

    // TODO use another method for ranged attack damage
    const { min, max } = calculatePossibleMeleeDamageRange(attackInitiator);
    const initiatorDamage = Phaser.Math.RND.integerInRange(min, max);

    let damage = initiatorDamage - targetArmorResistance;
    let damageModifier = 0;

    if (attackResult.isCriticalSuccess()) {
        damageModifier += damage; // x2
    }

    if (defenceResult !== null && defenceResult.isCriticalFailure()) {
        damageModifier += damage; // x2
    }

    return damage + damageModifier;
}

export function calculatePossibleMeleeDamageRange(character: Character): Damage {
    let weaponDamage: Damage = { min: 0, max: 0 };

    if (character.getEquipment().hasWeapon()) {
        weaponDamage = character.getEquipment().getWeapon().getDamage()
    }

    return {
        min: character.getAttributes().getStrength() + weaponDamage.min,
        max: character.getAttributes().getStrength() + weaponDamage.max,
    };
}
