import Equipment from '@game/Core/RPGSystem/Character/Equipment/Equipment';
import { Action, InitialAttributes, PlayerUIStats } from '@game/Types';
import Character from '@game/Models/Characters/Character';
import Backpack from '@game/Core/RPGSystem/Character/Equipment/Backpack';

type Rechargable = {
    balance: number,
    reserve: number,
};

// TODO strength, agility, and skills could be increased via level-up
class Attributes {
    private readonly character: Character;
    private readonly actionPoints: Rechargable;
    private readonly healthPoints: Rechargable;
    private readonly movePoints: number;
    private readonly strength: number;
    private readonly agility: number;
    private readonly luck: number;
    private readonly meleeSkill: number;
    private readonly shootingSkill: number;
    private readonly blockingSkill: number;
    private readonly equipment: Equipment;
    private readonly backpack: Backpack;

    public constructor(character: Character, attributes: InitialAttributes, equipment: Equipment) {
        this.character = character;
        this.actionPoints = { balance: attributes.actionPoints, reserve: attributes.actionPoints };
        this.healthPoints = { balance: attributes.healthPoints, reserve: attributes.healthPoints };
        this.movePoints = attributes.movePoints;
        this.strength = attributes.strength;
        this.agility = attributes.agility;
        this.luck = attributes.luck !== undefined ? attributes.luck : 0;
        this.meleeSkill = attributes.meleeSkill !== undefined ? attributes.meleeSkill : 0;
        this.shootingSkill = attributes.shootingSkill !== undefined ? attributes.shootingSkill : 0;
        this.blockingSkill = attributes.blockingSkill !== undefined ? attributes.blockingSkill : 0;
        this.equipment = equipment;
        this.backpack = new Backpack(this.character);

        this.character.scene.time.delayedCall(10, this.emitAttributesUpdatedEvent.bind(this));
    }

    // Must be called before character's turn
    public rechargeBeforeTurn(): this {
        this.rechargeActionPoints();
        return this;
    }

    public getActionPoints(): number {
        return this.actionPoints.balance;
    }

    public canPerformAction(action: Action): boolean {
        return this.actionPoints.balance >= action.cost;
    }

    public decreaseActionPoints(value: number = 1): this {
        this.actionPoints.balance -= value;

        if (this.actionPoints.balance < 0) {
            this.actionPoints.balance = 0;
        }

        this.emitAttributesUpdatedEvent();

        return this;
    }

    public nullifyActionPoints(): this {
        this.actionPoints.balance = 0;
        return this;
    }

    public rechargeActionPoints(): this {
        this.actionPoints.balance = this.actionPoints.reserve;

        this.emitAttributesUpdatedEvent();

        return this;
    }

    public getHealthPoints(): number {
        return this.healthPoints.balance;
    }

    public increaseHealthPoints(value: number): this {
        this.healthPoints.balance += value;

        if (this.healthPoints.balance > this.healthPoints.reserve) {
            this.healthPoints.balance = this.healthPoints.reserve;
        }

        this.emitAttributesUpdatedEvent();

        return this;
    }

    public decreaseHitPoints(value: number): this {
        this.healthPoints.balance -= value;

        if (this.healthPoints.balance < 0) {
            this.healthPoints.balance = 0;
        }

        this.emitAttributesUpdatedEvent();

        return this;
    }

    public getHealthPointsAsPercent(): number {
        return Math.floor(this.healthPoints.balance / this.healthPoints.reserve * 100);
    }

    public getMovePoints(): number {
        return this.movePoints;
    }

    public getStrength(): number {
        return this.strength;
    }

    public getAgility(): number {
        return this.agility;
    }

    public getLuck(): number {
        return this.luck;
    }

    public getMeleeSkill(): number {
        return this.meleeSkill;
    }

    public getShootingSkill(): number {
        return this.shootingSkill;
    }

    public getBlockingSkill(): number {
        return this.blockingSkill;
    }

    public getEquipment(): Equipment {
        return this.equipment;
    }

    public getBackpack(): Backpack {
        return this.backpack;
    }

    private emitAttributesUpdatedEvent(): void {
        const updatedStats: PlayerUIStats = {
            hp: this.getHealthPointsAsPercent(),
            ap: this.actionPoints,
        };

        this.character
            .getScene()
            .getEventEmitter()
            .emitCharacterAttributesUpdated(this.character, updatedStats);
    }
}

export default Attributes;
