import Weapon from '@game/Core/RPGSystem/Character/Equipment/Weapons/Weapon';
import Armor from '@game/Core/RPGSystem/Character/Equipment/Armors/Armor';

// TODO add backpack
class Equipment {
    private weapon: Weapon | null = null;
    private armor: Armor | null = null;
    private shield: boolean = false;

    public addWeapon(weapon: Weapon): this {
        this.weapon = weapon;
        return this;
    }

    public hasWeapon(): boolean {
        return this.weapon !== null;
    }

    public getWeapon(): Weapon {
        if (!this.hasWeapon()) {
            throw new Error('Unexpected situation');
        }

        return this.weapon as Weapon;
    }

    public addArmor(armor: Armor): this {
        this.armor = armor;
        return this;
    }

    public hasArmor(): boolean {
        return this.armor !== null;
    }

    public getArmor(): Armor {
        if (!this.hasArmor()) {
            throw new Error('Unexpected situation');
        }

        return this.armor as Armor;
    }

    public hasShield(): boolean {
        return this.shield;
    }
}

export default Equipment;
