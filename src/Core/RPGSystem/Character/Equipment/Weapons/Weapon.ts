import { Damage, WeaponType } from '@game/Types';

abstract class Weapon {
    protected name: string;
    protected damage: Damage;
    protected speedPenalty: number;

    protected constructor(name: string, damage: Damage, speedPenalty: number = 0) {
        this.name = name;
        this.damage = damage;
        this.speedPenalty = speedPenalty;
    }

    public abstract getType(): WeaponType;

    public getName(): string {
        return this.name;
    }

    public getDamage(): Damage {
        return this.damage;
    }

    public getSpeedPenalty(): number {
        return this.speedPenalty;
    }
}

export default Weapon;
