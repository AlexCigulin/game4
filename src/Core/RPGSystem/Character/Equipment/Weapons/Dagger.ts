import Weapon from '@game/Core/RPGSystem/Character/Equipment/Weapons/Weapon';
import { WeaponType } from '@game/Types';

class Dagger extends Weapon {
    public constructor() {
        super('Dagger', { min: 1, max: 2 }, 0);
    }

    public getType(): WeaponType {
        return WeaponType.MELEE;
    }
}

export default Dagger;
