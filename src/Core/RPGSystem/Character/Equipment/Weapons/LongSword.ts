import Weapon from '@game/Core/RPGSystem/Character/Equipment/Weapons/Weapon';
import { WeaponType } from '@game/Types';

class LongSword extends Weapon {
    public constructor() {
        super('Long Sword', { min: 3, max: 5 }, 1);
    }

    public getType(): WeaponType {
        return WeaponType.MELEE;
    }
}

export default LongSword;
