import Weapon from '@game/Core/RPGSystem/Character/Equipment/Weapons/Weapon';
import { WeaponType } from '@game/Types';

class ShortSword extends Weapon {
    public constructor() {
        super('Short Sword', { min: 2, max: 3 }, 0);
    }

    public getType(): WeaponType {
        return WeaponType.MELEE;
    }
}

export default ShortSword;
