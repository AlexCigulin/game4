import Weapon from '@game/Core/RPGSystem/Character/Equipment/Weapons/Weapon';
import { WeaponType } from '@game/Types';

class LongSword extends Weapon {
    public constructor() {
        super('Two-handed Weapon', { min: 4, max: 6 }, 2);
    }

    public getType(): WeaponType {
        return WeaponType.MELEE;
    }
}

export default LongSword;
