import { Resistance } from '@game/Types';

abstract class Armor {
    protected name: string;
    protected resistance: Resistance;

    protected constructor(name: string, resistance: Resistance) {
        this.name = name;
        this.resistance = resistance;
    }

    public getResistance(): Resistance {
        return this.resistance;
    }
}

export default Armor;
