import Armor from '@game/Core/RPGSystem/Character/Equipment/Armors/Armor';

class PlateArmor extends Armor {
    public constructor() {
        super('Plate Armor', { min: 3, max: 5 });
    }
}

export default PlateArmor;
