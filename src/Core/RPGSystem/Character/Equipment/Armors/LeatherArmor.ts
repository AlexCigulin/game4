import Armor from '@game/Core/RPGSystem/Character/Equipment/Armors/Armor';

class LeatherArmor extends Armor {
    public constructor() {
        super('Leather Armor', { min: 1, max: 2 });
    }
}

export default LeatherArmor;
