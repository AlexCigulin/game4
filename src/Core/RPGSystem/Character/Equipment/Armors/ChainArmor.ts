import Armor from '@game/Core/RPGSystem/Character/Equipment/Armors/Armor';

class ChainArmor extends Armor {
    public constructor() {
        super('Chain Armor', { min: 2, max: 3 });
    }
}

export default ChainArmor;
