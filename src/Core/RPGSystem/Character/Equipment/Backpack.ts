import Loot from '@game/Models/Loot/Loot';
import Character from '@game/Models/Characters/Character';

type ItemMap = {
    [id: string]: Loot;
};

class Backpack {
    private readonly character: Character;
    private readonly loot: ItemMap

    public constructor(character: Character) {
        this.character = character;
        this.loot = {};
    }

    public getItems(): Loot[] {
        return Object.values(this.loot);
    }

    public addItem(loot: Loot): void {
        this.loot[loot.getId()] = loot;
    }

    public removeItem(id: string): Loot {
        if (this.loot[id] === undefined) {
            throw new Error('Item not found: ' + id);
        }

        const item = this.loot[id];

        delete this.loot[id];

        return item;
    }

    public useItem(id: string): void {
        const item = this.removeItem(id);
        item.useBy(this.character);
    }
}

export default Backpack;
