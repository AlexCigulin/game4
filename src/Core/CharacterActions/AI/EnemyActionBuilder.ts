import MapManager from '@game/Map/MapManager';
import { Action, ActionType, TilePosition } from '@game/Types';
import { areTilePositionsEquals, getAdjacentTilePositionRange } from '@game/Map/Helper';
import Enemy from '@game/Models/Characters/Enemy';

class EnemyActionBuilder {
    private mapManager: MapManager;

    public constructor(mapManager: MapManager) {
        this.mapManager = mapManager;
    }

    public build(initiator: Enemy): Action[] {
        const position = this.mapManager.getGrid().getModelPosition(initiator);
        const positionRange = getAdjacentTilePositionRange(
            position,
            initiator.getAttributes().getMovePoints(),
            this.mapManager.getSizeInTiles()
        );

        const actions: Action[] = [];

        for (let y = positionRange.fromY; y <= positionRange.toY; y++) {
            for (let x = positionRange.fromX; x <= positionRange.toX; x++) {
                const action = this.buildAction(initiator, position, { x, y });

                if (action && initiator.getAttributes().canPerformAction(action)) {
                    actions.push(action);
                }
            }
        }

        return actions;
    }

    private buildAction(
        initiator: Enemy,
        initiatorPosition: TilePosition,
        positionToCheck: TilePosition
    ): Action | null {
        if (areTilePositionsEquals(initiatorPosition, positionToCheck)) {
            return null;
        }

        if (this.mapManager.getGrid().isFreeOnPosition(positionToCheck)) {
            return this.buildActionWithPath(initiator, initiatorPosition, ActionType.MOVE, positionToCheck, false);
        }

        const targetModel = this.mapManager.getGrid().getModelByPosition(positionToCheck);

        if (targetModel === null || targetModel.isEnemy()) {
            return null;
        }

        return this.buildActionWithPath(
            initiator,
            initiatorPosition,
            targetModel.isPlayer() ? ActionType.ATTACK : ActionType.INTERACT,
            positionToCheck,
            targetModel.isPlayer()
        );
    }

    private buildActionWithPath(
        initiator: Enemy,
        initiatorPosition: TilePosition,
        type: ActionType,
        targetPosition: TilePosition,
        attackTarget: boolean
    ): Action | null {
        const path = this.mapManager.findPath(initiatorPosition, targetPosition, attackTarget);

        if (!path || !path.length || path.length > initiator.getAttributes().getMovePoints()) {
            return null;
        }

        return {
            initiator,
            target: targetPosition,
            path,
            type,
            cost: path.length > 1 && attackTarget ? 2 : 1,
        };
    }
}

export default EnemyActionBuilder;
