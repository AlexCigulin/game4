import CharacterController from '@game/Core/CharacterActions/CharacterController';
import Enemy from '@game/Models/Characters/Enemy';
import { ActionType, GameScene } from '@game/Types';
import EnemyActionBuilder from '@game/Core/CharacterActions/AI/EnemyActionBuilder';

class EnemyController extends CharacterController {
    private actionBuilder: EnemyActionBuilder;
    private enemyStack: Enemy[];
    private activeEnemyIndex!: number;
    private endOfTurnReached!: boolean;

    public constructor(scene: GameScene) {
        super(scene);

        this.actionBuilder = new EnemyActionBuilder(this.scene.getMapManager());
        this.enemyStack = [];
    }

    public update(): void {
        if (this.activeEnemyIndex + 1 > this.enemyStack.length) {
            this.endOfTurnReached = true;
            return;
        }

        const activeEnemy = this.enemyStack[this.activeEnemyIndex];

        if (activeEnemy.getAttributes().getActionPoints() === 0 && !activeEnemy.isBusy()) {
            this.activeEnemyIndex += 1;
        } else if (activeEnemy.isIdle()) {
            this.doAction();
        }
    }

    public startTurn(): void {
        this.turnStarted = true;
        this.enemyStack = this.scene
            .getModelContainer()
            .getEnemies()
            .filter((enemy: Enemy): boolean => {
                return this.camera.worldView.contains(enemy.x, enemy.y);
            });

        this.enemyStack.forEach((enemy: Enemy): void => { enemy.getAttributes().rechargeBeforeTurn() });

        this.activeEnemyIndex = 0;
        this.endOfTurnReached = false;
    }

    public endTurn(): void {
        super.endTurn();
        this.enemyStack = [];
    }

    public inAction(): boolean {
        return !this.endOfTurnReached;
    }

    private doAction(): void {
        const activeEnemy = this.enemyStack[this.activeEnemyIndex];

        const actions = this.actionBuilder.build(activeEnemy);

        if (!actions.length) {
            activeEnemy.getAttributes().nullifyActionPoints();
            return;
        }

        for (let i = 0; i < actions.length; i++) {
            const action = actions[i];

            if (action.type === ActionType.ATTACK) {
                this.executeAction(action);
                return;
            }
        }

        const randomAction = actions[Math.floor(Math.random() * actions.length)];

        this.executeAction(randomAction);
    }
}

export default EnemyController;
