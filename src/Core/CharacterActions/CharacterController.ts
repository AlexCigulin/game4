import { Cameras } from 'phaser';
import { Action, ActionType, Callback, GameScene, TargetPosition } from '@game/Types';
import Character from '@game/Models/Characters/Character';
import config from '@game/Config';
import { getOppositeDirection, isDiagonalDirection } from '@game/Map/Helper';
import MeleeCombatHandler from '@game/Core/RPGSystem/ActionHandlers/MeleeCombatHandler';
import EventEmitter from '@game/Events/EventEmitter';

abstract class CharacterController {
    protected scene: GameScene;
    protected eventEmitter: EventEmitter;
    protected camera: Cameras.Scene2D.Camera;
    protected actions: Action[] | null;
    protected turnStarted: boolean;
    protected meleeCombatHandler: MeleeCombatHandler;

    protected constructor(scene: GameScene) {
        this.scene = scene;
        this.eventEmitter = this.scene.getEventEmitter();
        this.camera = this.scene.cameras.main;
        this.actions = null;
        this.turnStarted = false;

        this.meleeCombatHandler = new MeleeCombatHandler(this.scene.getEventEmitter());
    }

    public abstract inAction(): boolean;

    public update(): void {
        // Empty
    }

    public startTurn(): void {
        this.turnStarted = true;
    }

    public endTurn(): void {
        this.turnStarted = false;
        this.actions = null;
    }

    protected onActionComplete(character: Character, actionCost: number = 1): void {
        this.scene.time.delayedCall(400, () => {
            character
                .setIdle()
                .getAttributes()
                .decreaseActionPoints(actionCost);
        });
    }

    protected executeAction(action: Action): void {
        action.initiator.setBusy();

        switch (action.type) {
            case ActionType.SELF:
                return this.selfAction();
            case ActionType.INTERACT:
                return this.interactAction(action.path as TargetPosition[]);
            case ActionType.MOVE:
                return this.moveAction(action.initiator, action.path as TargetPosition[]);
            case ActionType.ATTACK:
                return this.attackAction(action.initiator, action.path as TargetPosition[]);
        }
    }

    protected selfAction(): void {
        throw new Error('Self action is not implemented');
    }

    protected interactAction(path: TargetPosition[]): void {
        throw new Error('Interact action is not implemented');
    }

    protected moveAction(character: Character, path: TargetPosition[], afterMoveHandler?: Callback): void {
        const timeline = this.scene.tweens.createTimeline();
        const previousDirections: Array<string | null> = [];
        const walkingDuration = character.getAnimationConfig().walkDuration ?? null;
        const walkingFrameRate = character.getAnimationConfig().walkFrameRate;
        let onWalkEnd: Callback;

        for (let i = 0; i < path.length; i++) {
            const previousDirection =  previousDirections.length ? previousDirections[i - 1] : null;
            const currentDirection = path[i].direction;
            const isLastStep = i + 1 === path.length;
            let duration = walkingDuration ?? walkingFrameRate;

            if (isDiagonalDirection(currentDirection)) {
                duration *= 1.5;
            }

            timeline.add({
                targets: character,
                duration: duration,
                useFrames: walkingDuration === null,
                repeat: 0,
                yoyo: false,
                ease: 'Linear',
                x: path[i].x * config.gridCellSize + (config.gridCellSize / 2),
                y: path[i].y * config.gridCellSize + (config.gridCellSize / 2),
                onStart: () => {
                    character.updateDepth(path[i].y);

                    if (previousDirection === null || previousDirection !== currentDirection) {
                        const walk = character
                            .updateDirection(currentDirection)
                            .playWalk();

                        if (onWalkEnd === undefined) {
                            onWalkEnd = walk;
                        }
                    }
                },
                onComplete: () => {
                    this.scene.getMapManager().getGrid().updateModelPosition(character, { x: path[i].x, y: path[i].y });

                    if (isLastStep) {
                        character.playIdle();
                    }
                },
            });

            previousDirections.push(currentDirection);
        }

        timeline
            .setCallback('onStart', () => {
                character.playActivated();
            })
            .setCallback('onComplete', () => {
                onWalkEnd && onWalkEnd();

                if (afterMoveHandler) {
                    afterMoveHandler();
                } else {
                    this.onActionComplete(character);
                }
            })
            .play();
    }

    protected attackAction(character: Character, path: TargetPosition[]): void {
        const movePath = [ ...path ];
        const targetPosition = movePath.pop() as TargetPosition;
        const targetCharacter = this.scene.getMapManager().getGrid().getModelByPosition(targetPosition) as Character;

        const onComplete = () => {
            this.onActionComplete(character, movePath.length > 0 ? 2 : 1);
        };

        const attackHandler: Callback = () => {
            character
                .updateDirection(targetPosition.direction)
                .playAttack(() => {
                    character.playIdle();
                });

            const wasInjured = this.meleeCombatHandler.handle(character, targetCharacter);

            if (wasInjured) {
                targetCharacter.playAttackImpact();
            }

            targetCharacter
                .setHoldsPosition(false)
                .updateDirection(getOppositeDirection(targetPosition.direction));

            if (targetCharacter.isDead()) {
                targetCharacter.playDie(() => {
                    this.scene.getModelContainer().removeModel(targetCharacter.getId());
                    onComplete();
                });
            } else {
                targetCharacter.playDefend(() => {
                    targetCharacter.playIdle();
                    this.scene.time.delayedCall(100, () => { onComplete() });
                });
            }
        };

        this.moveAction(character, movePath, attackHandler);
    }
}

export default CharacterController;
