import CharacterController from '@game/Core/CharacterActions/CharacterController';
import { Input } from 'phaser';
import Player from '@game/Models/Characters/Player';
import {
    Action,
    Callback,
    ContextMenuConfig,
    ContextMenuItem,
    Direction,
    GameScene,
    NoticeType,
    TargetPosition,
    TilePosition
} from '@game/Types';
import { areTilePositionsEquals, getDirections } from '@game/Map/Helper';
import { directionToSymbol } from '@game/Helper';
import PlayerActionBuilder from '@game/Core/CharacterActions/Player/PlayerActionBuilder';
import Character from '@game/Models/Characters/Character';
import Loot from '@game/Models/Loot/Loot';

class PlayerController extends CharacterController {
    private readonly player: Player;
    private readonly actionBuilder: PlayerActionBuilder;

    public constructor(scene: GameScene) {
        super(scene);

        this.player = this.scene.getModelContainer().getPlayer();
        this.actionBuilder = new PlayerActionBuilder(this.player, this.scene.getMapManager());

        this.scene.input.on(Input.Events.POINTER_DOWN, this.handleClick.bind(this));
    }

    public startTurn() {
        super.startTurn();
        this.player.setHoldsPosition(false);
        this.player.getAttributes().rechargeBeforeTurn();
        this.buildAndShowActions();
    }

    public endTurn() {
        super.endTurn();
    }

    public isDead(): boolean {
        return this.player.isDead();
    }

    public inAction(): boolean {
        return this.player.getAttributes().getActionPoints() > 0 || this.player.isBusy();
    }

    protected selfAction(): void {
        const contextMenuItems: ContextMenuItem[] = [
            {
                title: 'Hold Position',
                onClick: () => {
                    this.eventEmitter.emitShowCharacterNotice(this.player, 'Defence +20%', NoticeType.GOOD);
                    this.player.setHoldsPosition(true);
                    this.player.getAttributes().nullifyActionPoints();
                    this.onActionComplete(this.player);
                },
            },
            {
                title: 'Change Direction',
                onClick: this.changeDirectionAction.bind(this),
            },
            // {
            //     title: 'Equipment',
            //     onClick: () => {
            //         console.log('TODO: open equipment');
            //         this.onActionCancel();
            //     },
            // },
            // {
            //     title: 'Character Info',
            //     onClick: () => {
            //         this.eventEmitter.emitOpenPlayerCharacteristics(this.player, this.onActionCancel.bind(this));
            //     },
            // }
        ];

        this.eventEmitter.emitShowContextMenu({
            showCancel: true,
            onCancel: this.onActionCancel.bind(this),
            items: contextMenuItems
        });
    }

    protected interactAction(path: TargetPosition[]): void {
        const movePath = [ ...path ];
        const targetPosition = movePath.pop() as TargetPosition;
        // TODO there will be other models to interact, not only loot:
        const targetModel = this.scene.getMapManager().getGrid().getModelByPosition(targetPosition) as Loot;

        const interactHandler: Callback = () => {
            this.eventEmitter.emitShowContextMenu({
                showCancel: false,
                onCancel: () => this.onActionComplete(this.player),
                items: [
                    {
                        title: 'Take',
                        onClick: () => {
                            this.moveAction(this.player, [ targetPosition ], () => {
                                targetModel.takeBy(this.player);
                                this.onActionComplete(this.player);
                            });
                        },
                    },
                    {
                        title: 'Leave',
                        onClick: () => {
                            this.onActionComplete(this.player);
                        },
                    },
                ]
            });
        };

        this.moveAction(this.player, movePath, interactHandler);
    }

    protected onActionComplete(character: Character, actionCost: number = 1): void {
        this.scene.time.delayedCall(200, () => {
            character
                .setIdle()
                .getAttributes()
                .decreaseActionPoints(actionCost);

            if (character.getAttributes().getActionPoints() > 0) {
                this.buildAndShowActions();
            }
        });
    }

    private onActionCancel(): void {
        this.player.setIdle();
        this.eventEmitter.emitMapMarkupShowActions(this.actions as Action[]);
    }

    private changeDirectionAction(): void {
        const items = getDirections(false).map((direction: Direction): ContextMenuItem => {
            return {
                title: directionToSymbol(direction),
                onClick: () => {
                    this.player
                        .updateDirection(direction)
                        .playIdle();

                    this.onActionCancel();
                },
            };
        });

        const config: ContextMenuConfig = {
            showCancel: false,
            onCancel: this.onActionCancel.bind(this),
            layout: 'horizontal',
            items,
        };

        this.eventEmitter.emitShowContextMenu(config);
    }

    private handleClick(pointer: Input.Pointer): void {
        if (this.player.isBusy() || !this.turnStarted) {
            return;
        }

        pointer.positionToCamera(this.camera);

        const { worldX, worldY } = pointer
        const tilePosition = this.scene.getMapManager().getTilePositionFromWorldXY(worldX, worldY);
        const action = this.getSelectedAction(tilePosition);

        if (action !== null) {
            this.eventEmitter.emitMapMarkupHideActions();
            this.executeAction(action);
        }
    }

    private getSelectedAction(clickedTile: TilePosition): Action | null {
        if (this.actions === null) {
            throw new Error('Unexpected error: there are no available actions');
        }

        for (let i = 0; i < this.actions.length; i++) {
            const action = this.actions[i];

            if (areTilePositionsEquals(action.target, clickedTile)) {
                return action;
            }
        }

        return null;
    }

    private buildAndShowActions(): void {
        this.actions = this.actionBuilder.build();
        this.eventEmitter.emitMapMarkupShowActions(this.actions);
    }
}

export default PlayerController;
