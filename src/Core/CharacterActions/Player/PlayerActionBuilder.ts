import MapManager from '@game/Map/MapManager';
import Player from '@game/Models/Characters/Player';
import { Action, ActionType, PositionRange, TilePosition } from '@game/Types';
import { areTilePositionsEquals, getAdjacentTilePositionRange } from '@game/Map/Helper';

class PlayerActionBuilder {
    private readonly player: Player;
    private playerPosition: TilePosition | null;
    private readonly mapManager: MapManager;

    public constructor(player: Player, mapManager: MapManager) {
        this.player = player;
        this.playerPosition = null;
        this.mapManager = mapManager;
    }

    public build(): Action[] {
        this.checkPlayerPosition();

        const positionRange = this.getAvailablePositionRange();
        const actions: Action[] = [];

        for (let y = positionRange.fromY; y <= positionRange.toY; y++) {
            for (let x = positionRange.fromX; x <= positionRange.toX; x++) {
                const action = this.buildActionIfPossible({ x, y });

                if (action && this.player.getAttributes().canPerformAction(action)) {
                    actions.push(action);
                }
            }
        }

        this.playerPosition = null;

        return actions;
    }

    private buildActionIfPossible(positionToCheck: TilePosition): Action | null {
        const playerPosition = this.playerPosition as TilePosition;

        if (areTilePositionsEquals(playerPosition, positionToCheck)) {
            return {
                initiator: this.player,
                target: playerPosition,
                type: ActionType.SELF,
                cost: 0,
            };
        }

        if (this.mapManager.getGrid().isFreeOnPosition(positionToCheck)) {
            return this.buildActionWithPath(ActionType.MOVE, positionToCheck, true);
        }

        const targetModel = this.mapManager.getGrid().getModelByPosition(positionToCheck);

        if (targetModel === null) {
            return null;
        }

        return this.buildActionWithPath(
            targetModel.isEnemy() ? ActionType.ATTACK : ActionType.INTERACT,
            positionToCheck,
            false
        );
    }

    private buildActionWithPath(type: ActionType, targetPosition: TilePosition, moveOnly: boolean): Action | null {
        const path = this.mapManager.findPath(this.playerPosition as TilePosition, targetPosition, !moveOnly);

        if (!path || path.length === 0 || path.length > this.player.getAttributes().getMovePoints()) {
            return null;
        }

        return {
            initiator: this.player,
            target: targetPosition,
            path,
            type,
            cost: path.length > 1 && type === ActionType.ATTACK ? 2 : 1,
        };
    }

    private checkPlayerPosition(): void {
        this.playerPosition = this.mapManager.getGrid().getModelPosition(this.player);
    }

    private getAvailablePositionRange(): PositionRange {
        return getAdjacentTilePositionRange(
            this.playerPosition as TilePosition,
            this.player.getAttributes().getMovePoints(),
            this.mapManager.getSizeInTiles()
        );
    }
}

export default PlayerActionBuilder;
