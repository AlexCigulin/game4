import PlayerController from '@game/Core/CharacterActions/Player/PlayerController';
import CharacterController from '@game/Core/CharacterActions/CharacterController';
import EnemyController from '@game/Core/CharacterActions/AI/EnemyController';
import { GameScene } from '@game/Types';

enum Phase {
    PLAYER_TURN = 'player',
    ENEMIES_TURN = 'enemy'
}

class GameLoop {
    private readonly scene: GameScene;
    private readonly playerController: PlayerController;
    private readonly enemyController: EnemyController;
    private initialized: boolean;
    private currentPhase!: Phase;
    private activeController!: CharacterController;
    private gameIsOver: boolean;

    public constructor(scene: GameScene) {
        this.scene = scene;
        this.playerController = new PlayerController(this.scene);
        this.enemyController = new EnemyController(this.scene);
        this.initialized = false;
        this.gameIsOver = false;
    }

    public update(): void {
        if (this.gameIsOver) {
            // TODO
            console.log('GAME IS OVER');
            return;
        }

        if (!this.initialized) {
            this.init();
        }

        if (this.activeController.inAction()) {
            this.activeController.update();
        } else {
            this.activeController.endTurn();
            this.switchPhase();
        }
    }

    private switchPhase(): void {
        if (this.currentPhase === Phase.PLAYER_TURN) {
            this.currentPhase = Phase.ENEMIES_TURN;
            this.activeController = this.enemyController;
        } else if (this.currentPhase === Phase.ENEMIES_TURN) {
            if (this.playerController.isDead()) {
                this.gameIsOver = true;
                return;
            }

            this.currentPhase = Phase.PLAYER_TURN;
            this.activeController = this.playerController;
        }

        this.activeController.startTurn();
    }

    private init(): void {
        this.initialized = true;
        this.currentPhase = Phase.PLAYER_TURN;
        this.activeController = this.playerController;
        this.activeController.startTurn();
    }
}

export default GameLoop;
