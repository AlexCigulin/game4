import { GameConfig } from '@game/Types';
import scenes from '@game/Scenes';

const config: GameConfig = {
    title: 'Game 4',
    width: 800,
    height: 600,
    type: Phaser.WEBGL,
    parent: 'game',
    backgroundColor: '#000000',
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
        },
    },
    render: {
        antialiasGL: false,
        pixelArt: true,
    },
    canvasStyle: `display: block; width: 800px; height: 600px; margin: 0 auto; border: 1px solid #49453a;`,
    autoFocus: true,
    audio: {
        disableWebAudio: false,
    },
    scene: scenes,
    gridCellSize: 16,
    assetsBaseUrl: '../assets/',
    debugGrid: false,
    debugObstacles: false,
    debugBattle: false,
};

export default config;
