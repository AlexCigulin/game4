import { Types, Scene } from 'phaser';
import Character from '@game/Models/Characters/Character';
import MapManager from '@game/Map/MapManager';
import ModelContainer from '@game/Models/ModelContainer';
import EventEmitter from '@game/Events/EventEmitter';
import CharacterSoundFXProxy from '@game/Sounds/CharacterSoundFXProxy';
import LootSoundFXProxy from '@game/Sounds/LootSoundFXProxy';

export type Callback = (...args: any[]) => void;

export type GameConfig = Types.Core.GameConfig & {
    gridCellSize: number;
    assetsBaseUrl: string;
    debugGrid: boolean;
    debugObstacles: boolean;
    debugBattle: boolean;
};

export interface GameScene extends Scene {
    getEventEmitter(): EventEmitter;
    getMapManager(): MapManager;
    getModelContainer(): ModelContainer;
    getCharacterSoundFXProxy(): CharacterSoundFXProxy;
    getLootSoundFXProxy(): LootSoundFXProxy;
}

export enum Direction {
    N = 'north',
    NE = 'northEast',
    E = 'east',
    SE = 'southEast',
    S = 'south',
    SW = 'southWest',
    W = 'west',
    NW = 'northWest',
}

export type AbsolutePosition = {
    x: number;
    y: number;
};

export type TilePosition = {
    x: number;
    y: number;
};

export type TargetPosition = TilePosition & {
    direction: Direction;
};

export type Size = {
    width: number;
    height: number;
};

export type PositionRange = {
    fromX: number;
    toX: number;
    fromY: number;
    toY: number;
};

export enum ModelType {
    PLAYER = 'player',
    ENEMY = 'enemy',
    LOOT = 'loot',
}

export interface ModelInterface {
    x: number;
    y: number;
    getId(): string;
    getDisplayedName(): string;
    getType(): ModelType;
    isCharacter(): boolean;
    isPlayer(): boolean;
    isEnemy(): boolean;
}

export enum CharacterState {
    IDLE = 'idle',
    BUSY = 'busy',
    DEAD = 'dead',
}

export type InitialAttributes = {
    actionPoints: number,
    healthPoints: number;
    movePoints: number;
    strength: number;
    agility: number;
    luck?: number;
    meleeSkill?: number;
    shootingSkill?: number;
    blockingSkill?: number;
};

export type CharacterAnimationConfig = {
    idleFrameRate: number;
    walkFrameRate: number;
    walkDuration?: number;
    attackFrameRate: number;
    defendFrameRate: number;
    dieFrameRate: number;
};

export type CharacterSoundConfig = {
    activated?: string;
    walk: string;
    attackMelee: string;
    attackImpact: string;
    die: string;
};

export type LootSoundConfid = {
    onTake: string;
    onUse: string;
};

export enum ActionType {
    MOVE = 'move',
    ATTACK = 'attack',
    INTERACT = 'interact',
    SELF = 'self',
}

export enum AttackType {
    MELEE = 'melee',
    RANGED = 'ranged',
}

export type Action = {
    cost: number;
    initiator: Character;
    target: TilePosition;
    path?: TargetPosition[];
    type: ActionType;
    attackType?: AttackType;
}

export enum NoticeType {
    NEUTRAL = 'neutral',
    BAD = 'bad',
    GOOD = 'good',
}

export enum WeaponType {
    MELEE = 'melee',
    RANGED = 'ranged',
}

export type Damage = {
    min: number;
    max: number;
};

export type Resistance = {
    min: number;
    max: number;
};

export type ContextMenuItem = {
    title: string;
    onClick: Callback;
};

export type ContextMenuLayout = 'vertical' | 'horizontal';

export type ContextMenuConfig = {
    items: ContextMenuItem[];
    showCancel: boolean;
    onCancel: Callback;
    layout?: ContextMenuLayout;
};

export type PlayerUIStats = {
    hp?: number;
    ap?: { balance: number, reserve: number };
}
