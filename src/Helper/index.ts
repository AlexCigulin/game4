import { Math } from 'phaser';
import { Direction } from '@game/Types';

export function normalizeBetweenTwoBounds(value: number, min: number, max: number): number {
    switch (true) {
        case value < min:
            return min;
        case value > max:
            return max;
        default:
            return value;
    }
}

export function randomPercentInRange(percent: number): boolean {
    return Math.RND.integerInRange(1, 100) <= percent;
}

export function title(text: string): string {
    const words = text.replace(/([A-Z])/g, " $1");
    return words.charAt(0).toUpperCase() + words.slice(1);
}

export function directionToSymbol(direction: Direction): string {
    switch (direction) {
        case Direction.NW:
            return '↖';
        case Direction.N:
            return '↑';
        case Direction.NE:
            return '↗';
        case Direction.E:
            return '→';
        case Direction.SE:
            return '↘';
        case Direction.S:
            return '↓';
        case Direction.SW:
            return '↙';
        case Direction.W:
            return '←';
        default:
            throw new Error();
    }
}
