import { Scene } from 'phaser';
import config from '@game/Config';

class LoadingScene extends Scene {
    public constructor() {
        super('loading-scene');
    }

    public preload(): void {
        this.load.baseURL = config.assetsBaseUrl;

        this.load.image('spritesheet', 'spritesheets/spritesheet.png');
        this.load.tilemapTiledJSON('dungeon', 'tilemaps/dungeon.json');

        this.load.spritesheet('hero', 'spritesheets/hero.png', { frameWidth: 128, frameHeight: 128 });
        this.load.spritesheet('zombie', 'spritesheets/zombie.png', { frameWidth: 128, frameHeight: 128 });
        this.load.spritesheet('skeleton', 'spritesheets/skeleton.png', { frameWidth: 128, frameHeight: 128 });

        this.load.spritesheet('candle', 'spritesheets/candle.png', { frameWidth: 16, frameHeight: 16 });
        this.load.spritesheet('candles', 'spritesheets/candles.png', { frameWidth: 16, frameHeight: 24 });
        this.load.spritesheet('potion', 'spritesheets/potion.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('torch', 'spritesheets/torch.png', { frameWidth: 16, frameHeight: 16 });

        this.load.image('lamp', 'sprites/lamp.png');

        this.load.audio('attack_impact1', ['sounds/fx/attack_impact1.wav']);
        this.load.audio('attack_impact2', ['sounds/fx/attack_impact2.wav']);
        this.load.audio('attack_impact_zombie1', ['sounds/fx/attack_impact_zombie1.wav']);
        this.load.audio('attack_melee1', ['sounds/fx/attack_melee1.wav']);
        this.load.audio('attack_melee2', ['sounds/fx/attack_melee2.wav']);
        this.load.audio('attack_melee3', ['sounds/fx/attack_melee3.wav']);
        this.load.audio('attack_melee4', ['sounds/fx/attack_melee4.wav']);
        this.load.audio('die1', ['sounds/fx/die1.wav']);
        this.load.audio('die_zombie1', ['sounds/fx/die_zombie1.wav']);
        this.load.audio('hold_position1', ['sounds/fx/hold_position1.wav']);
        this.load.audio('walk_160bpm1', ['sounds/fx/walk_160bpm1.wav']);
        this.load.audio('walk_130bpm1', ['sounds/fx/walk_130bpm1.wav']);
        this.load.audio('activated_zombie1', ['sounds/fx/activated_zombie1.wav']);
        this.load.audio('activated_zombie2', ['sounds/fx/activated_zombie2.wav']);

        this.load.audio('loot_take1', ['sounds/fx/loot_take1.wav']);
        this.load.audio('loot_use1', ['sounds/fx/loot_use1.wav']);
        this.load.audio('potion_take1', ['sounds/fx/potion_take1.wav']);
        this.load.audio('potion_use1', ['sounds/fx/potion_use1.wav']);

        this.load.audio('ui_click1', ['sounds/fx/ui_click1.wav']);

        this.load.audio('ambient', ['sounds/themes/ambient.wav']);
    }

    public create(): void {
        this.scene.start('main-scene');
        this.scene.start('ui-scene');
    }
}

export default LoadingScene;
