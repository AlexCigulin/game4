import LoadingScene from '@game/Scenes/LoadingScene';
import MainScene from '@game/Scenes/MainScene';
import UIScene from '@game/Scenes/UIScene';

export default [
    LoadingScene,
    MainScene,
    UIScene,
];
