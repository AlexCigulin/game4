import { Scene, Tilemaps } from 'phaser';
import config from '@game/Config';
import MapManager from '@game/Map/MapManager';
import Player from '@game/Models/Characters/Player';
import ModelContainer from '@game/Models/ModelContainer';
import GameLoop from '@game/Core/GameLoop';
import {
    DEPTH_DEBUG_LAYER,
    DEPTH_MAP_BASE_LAYER,
    DEPTH_MODEL,
    DEPTH_TOP_LAYER
} from '@game/Constants';
import MapMarkup from '@game/UI/MapMarkup';
import Zombie from '@game/Models/Characters/Zombie';
import { GameScene, ModelInterface } from '@game/Types';
import Candle from '@game/Models/Decorative/Candle';
import Lamp from '@game/Models/Decorative/Lamp';
import Candles from '@game/Models/Decorative/Candles';
import { randomPercentInRange } from '@game/Helper';
import Torch from '@game/Models/Decorative/Torch';
import EventEmitter from '@game/Events/EventEmitter';
import CharacterSoundFXProxy from '@game/Sounds/CharacterSoundFXProxy';
import Potion from '@game/Models/Loot/Potion';
import LootSoundFXProxy from '@game/Sounds/LootSoundFXProxy';

class MainScene extends Scene implements GameScene {
    private eventEmitter!: EventEmitter;
    private map!: Tilemaps.Tilemap;
    private tileset!: Tilemaps.Tileset;
    private baseLayer!: Tilemaps.TilemapLayer;
    private mapManager!: MapManager;
    private modelContainer!: ModelContainer;
    private gameLoop!: GameLoop;
    private characterSoundFXProxy!: CharacterSoundFXProxy;
    private lootSoundFXProxy!: LootSoundFXProxy;

    public constructor() {
        super('main-scene');
    }

    public getEventEmitter(): EventEmitter {
        return this.eventEmitter;
    }

    public getMapManager(): MapManager {
        return this.mapManager;
    }

    public getModelContainer(): ModelContainer {
        return this.modelContainer;
    }

    public getCharacterSoundFXProxy(): CharacterSoundFXProxy {
        return this.characterSoundFXProxy;
    }

    public getLootSoundFXProxy(): LootSoundFXProxy {
        return this.lootSoundFXProxy;
    }

    public create(): void {
        this.eventEmitter = new EventEmitter(this);

        this.initMap();
        this.initSounds();
        this.initModels();
        this.initCamera();
        this.initTools();
        this.initDebug();
    }

    public update(): void {
        this.gameLoop.update();
    }

    private initMap(): void {
        this.map = this.make.tilemap({
            key: 'dungeon',
            tileWidth: config.gridCellSize,
            tileHeight: config.gridCellSize
        });

        this.tileset = this.map.addTilesetImage('spritesheet');

        const layersCount = 5;

        for (let i = 1; i <= layersCount; i++) {
            this.map
                .createLayer('layer' + i, this.tileset, 0, 0)
                .setDepth(DEPTH_MAP_BASE_LAYER + i);
        }

        this.map
            .createLayer('layer-top', this.tileset, 0, 0)
            .setDepth(DEPTH_TOP_LAYER);

        this.baseLayer = this.map
            .createLayer('impassable', this.tileset, 0, 0)
            .setDepth(DEPTH_DEBUG_LAYER)
            .setAlpha(0);

        this.physics.world.setBounds(0, 0, this.baseLayer.width, this.baseLayer.height);
        this.mapManager = new MapManager(this.map, this.baseLayer);
    }

    private initSounds(): void {
        this.characterSoundFXProxy = new CharacterSoundFXProxy(this);
        this.lootSoundFXProxy = new LootSoundFXProxy(this);
        // this.sound.add('ambient', { loop: true }).play();
    }

    private initModels(): void {
        this.modelContainer = new ModelContainer(this);

        this.map.filterObjects('characters', (object: any, i: number): void => {
            const { x, y } = this.mapManager.getNormalizedWorldXY(object.x, object.y);
            let model: ModelInterface | null = null;

            if (object.name === 'player') {
                model = new Player(this, x, y);
            } else if (object.name === 'zombie') {
                model = new Zombie('zombie' + i, this, x, y);
            }

            if (model) {
                this.modelContainer.addModel(model);
            }
        });

        this.map.filterObjects('decorations', (object: any): void => {
            const { x, y } = this.mapManager.getNormalizedWorldXY(object.x, object.y);
            const tilePosition = this.mapManager.getTilePositionFromWorldXY(x, y);

            if (object.name === 'candle') {
                if (randomPercentInRange(20)) {
                    new Candles(this, x, y, tilePosition.y);
                } else {
                    new Candle(this, x, y, tilePosition.y);
                }
            } else if (object.name === 'lamp') {
                new Lamp(this, x, y, tilePosition.y);
            } else if (object.name === 'torch') {
                new Torch(this, x, y, tilePosition.y);
            }
        });

        this.map.filterObjects('loot', (object: any): void => {
            const { x, y } = this.mapManager.getNormalizedWorldXY(object.x, object.y);
            const tilePosition = this.mapManager.getTilePositionFromWorldXY(x, y);
            let model: ModelInterface | null = null;

            if (object.name === 'healthpotion') {
                model = new Potion(this, x, y, tilePosition.y);
            }

            if (model) {
                this.modelContainer.addModel(model);
            }
        });
    }

    private initCamera(): void {
        this.cameras.main.setSize(this.game.scale.width, this.game.scale.height);
        this.cameras.main.setZoom(2);
        this.cameras.main.startFollow(this.modelContainer.getPlayer(), true, 0.09, 0.09);
    }

    private initTools(): void {
        new MapMarkup(this);
        this.gameLoop = new GameLoop(this);
    }

    private initDebug(): void {
        if (config.debugGrid) {
            this.map
                .createLayer('debug', this.tileset, 0, 0)
                .setDepth(DEPTH_MODEL - 2)
                .setAlpha(0.1);
        }

        if (config.debugObstacles) {
            this.baseLayer.setAlpha(0.15);
        }
    }
}

export default MainScene;
