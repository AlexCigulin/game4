import { Scene } from 'phaser';
import PlayerStats from '@game/UI/PlayerStats';
import PositionedNotices from '@game/UI/PositionedNotices';
import ContextMenu from '@game/UI/ContextMenu';
import EventEmitter from '@game/Events/EventEmitter';
import PlayerCharacteristics from '@game/UI/PlayerCharacteristics';

class UIScene extends Scene {
    public constructor() {
        super('ui-scene');
    }

    public create(): void {
        const eventEmitter = new EventEmitter(this);

        new PlayerStats(this, eventEmitter);
        new PositionedNotices(this, eventEmitter);
        new ContextMenu(this, eventEmitter);
        new PlayerCharacteristics(this, eventEmitter);
    }
}

export default UIScene;
