const pathfinding = require('pathfinding');

export type Node = {
    x: number,
    y: number;
    walkable?: boolean;
};

export enum DiagonalMovement {
    ALWAYS = 1,
    NEVER = 2,
    IF_AT_MOST_ONE_OBSTACLE = 3,
    ONLY_WHEN_NO_OBSTACLES = 4,
}

export interface GridInterface {
    new(matrix: number[][]): GridInterface;
    setWalkableAt(x: number, y: number, setWalkable: boolean): void;
    isWalkableAt(x: number, y: number): boolean;
    getNeighbors(node: Node, diagonalMovement: DiagonalMovement): Node[];
}

export interface AStarFinderInterface {
    new(properties?: { diagonalMovement: DiagonalMovement }): AStarFinderInterface;
    findPath(fromX: number, fromY: number, toX: number, toY: number, grid: GridInterface): FindPathResult | null;
}

export type FindPathResult = Array<any> & {
    [index: number]: Array<[number, number]>;
};

export const Grid = pathfinding.Grid as GridInterface;
export const AStarFinder = pathfinding.AStarFinder as AStarFinderInterface;
