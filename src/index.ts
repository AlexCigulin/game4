import { Game } from 'phaser';
import gameConfig from '@game/Config';

declare global {
    interface Window {
        game: Game;
    }
}

window.game = new Game(gameConfig);
