// UI related events:
export const UI_MAP_MARKUP_SHOW_ACTIONS = 'scene.ui.mapMarkupShowActions';
export const UI_MAP_MARKUP_HIDE_ACTIONS = 'scene.ui.mapMarkupHideActions';

// Global (multi scene) events:
export const GLOBAL_CHARACTER_ATTRIBUTES_UPDATED = 'global.character.attributesUpdated';
export const GLOBAL_UI_SHOW_POSITIONED_NOTICE = 'global.ui.showPositionedNotice';
export const GLOBAL_UI_OPEN_CONTEXTMENU = 'global.ui.openContextMenu';
export const GLOBAL_UI_OPEN_PLAYER_CHARACTERISTICS = 'global.ui.openPlayerCharacteristics';
