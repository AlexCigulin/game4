import {
    AbsolutePosition,
    Action,
    Callback,
    ContextMenuConfig,
    NoticeType,
    PlayerUIStats
} from '@game/Types';
import { Scene, Events } from 'phaser';
import * as Event from '@game/Events/index';
import Character from '@game/Models/Characters/Character';
import Player from '@game/Models/Characters/Player';

// TODO should be refactored
class EventEmitter {
    private scene: Scene;

    public constructor(scene: Scene) {
        this.scene = scene;
    }

    public getSceneEmitter(): Events.EventEmitter {
        return this.scene.events;
    }

    public getGlobalEmitter(): Events.EventEmitter {
        return this.scene.game.events;
    }

    public emitMapMarkupShowActions(actions: Action[]): void {
        this.getSceneEmitter().emit(Event.UI_MAP_MARKUP_SHOW_ACTIONS, actions);
    }

    public onMapMarkupShowActions(callback: Callback): void {
        this.getSceneEmitter().on(Event.UI_MAP_MARKUP_SHOW_ACTIONS, callback);
    }

    public emitMapMarkupHideActions(): void {
        this.getSceneEmitter().emit(Event.UI_MAP_MARKUP_HIDE_ACTIONS);
    }

    public onMapMarkupHideActions(callback: Callback): void {
        this.getSceneEmitter().on(Event.UI_MAP_MARKUP_HIDE_ACTIONS, callback);
    }

    public emitShowContextMenu(config: ContextMenuConfig): void {
        this.getGlobalEmitter().emit(Event.GLOBAL_UI_OPEN_CONTEXTMENU, config)
    }

    public onShowContextMenu(callback: Callback): void {
        this.getGlobalEmitter().on(Event.GLOBAL_UI_OPEN_CONTEXTMENU, callback)
    }

    public emitCharacterAttributesUpdated(character: Character, attributes: PlayerUIStats): void {
        this.getGlobalEmitter().emit(Event.GLOBAL_CHARACTER_ATTRIBUTES_UPDATED, character, attributes);
    }

    public onCharacterAttributesUpdated(callback: Callback): void {
        this.getGlobalEmitter().on(Event.GLOBAL_CHARACTER_ATTRIBUTES_UPDATED, callback);
    }

    public emitShowCharacterNotice(character: Character, text: string, type?: NoticeType): void {
        const camera = this.scene.cameras.main;

        const position: AbsolutePosition = {
            x: (character.x - camera.worldView.x) * camera.zoom,
            y: (character.y - camera.worldView.y) * camera.zoom,
        };

        this.getGlobalEmitter().emit(Event.GLOBAL_UI_SHOW_POSITIONED_NOTICE, position, text, type);
    }

    public onShowPositionedNotice(callback: Callback): void {
        this.getGlobalEmitter().on(Event.GLOBAL_UI_SHOW_POSITIONED_NOTICE, callback);
    }

    public emitOpenPlayerCharacteristics(player: Player, onClose: Callback): void {
        this.getGlobalEmitter().emit(Event.GLOBAL_UI_OPEN_PLAYER_CHARACTERISTICS, player, onClose);
    }

    public onOpenPlayerCharacteristics(callback: Callback): void {
        this.getGlobalEmitter().on(Event.GLOBAL_UI_OPEN_PLAYER_CHARACTERISTICS, callback);
    }
}

export default EventEmitter;