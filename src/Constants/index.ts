export const DEPTH_MAP_BASE_LAYER = 1;
export const DEPTH_MARKUP_AVAILABLE_ACTIONS = 999;
export const DEPTH_MODEL = 1000;
export const DEPTH_TOP_LAYER = 10000;
export const DEPTH_MARKUP_NOTICE = 10001;
export const DEPTH_DEBUG_LAYER = 10001;
export const DEPTH_UI = 11000;

export const CRITICAL_SUCCESS_THRESHOLD = 5;
export const CRITICAL_FAILURE_THRESHOLD = 96;
